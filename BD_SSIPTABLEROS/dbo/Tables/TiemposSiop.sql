﻿CREATE TABLE [dbo].[TiemposSiop] (
    [IdIntervencion]       INT             NOT NULL,
    [Activo]               VARCHAR (10)    NOT NULL,
    [NormalDias]           DECIMAL (18, 2) NULL,
    [NormalPorciento]      DECIMAL (18, 2) NULL,
    [EsperasDias]          DECIMAL (18, 2) NULL,
    [EsperasPorciento]     DECIMAL (18, 2) NULL,
    [ProblemasDias]        DECIMAL (18, 2) NULL,
    [ProblemasPorciento]   DECIMAL (18, 2) NULL,
    [AdicionalesDias]      DECIMAL (18, 2) NULL,
    [AdicionalesPorciento] DECIMAL (18, 2) NULL,
    [TotalDias]            DECIMAL (18, 2) NULL,
    [TotalPorciento]       DECIMAL (18, 2) NULL,
    CONSTRAINT [PK_TiemposSiop] PRIMARY KEY CLUSTERED ([IdIntervencion] ASC, [Activo] ASC)
);

