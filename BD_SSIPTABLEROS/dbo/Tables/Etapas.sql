﻿CREATE TABLE [dbo].[Etapas] (
    [IdIntervencion] INT           NOT NULL,
    [Activo]         VARCHAR (10)  NOT NULL,
    [IdEtapa]        INT           NULL,
    [Etapa]          VARCHAR (500) NULL,
    [TotalRP]        INT           NULL,
    [Peogramado]     INT           NULL,
    [Normal]         INT           NULL,
    [Esperas]        INT           NULL,
    [Problemas]      INT           NULL,
    CONSTRAINT [PK_Etapas] PRIMARY KEY CLUSTERED ([IdIntervencion] ASC, [Activo] ASC)
);

