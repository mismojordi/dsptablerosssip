﻿CREATE TABLE [dbo].[imagenesprograma] (
    [idimagenesprograma]    INT           NOT NULL,
    [RutaOriginal]          VARCHAR (500) NULL,
    [RutaRecortada]         VARCHAR (500) NULL,
    [idProgramaActividades] INT           NULL,
    [Tipo]                  VARCHAR (250) NULL,
    [FecCreacion]           DATETIME      NULL,
    [UsrCreacion]           VARCHAR (50)  NULL,
    [FecMod]                DATE          NULL,
    [UsrMod]                VARCHAR (50)  NULL,
    [Estado]                TEXT          NULL,
    [FecEstado]             DATE          NULL,
    [Activo]                VARCHAR (10)  NOT NULL,
    [FechaActualizacion]    DATETIME      NULL,
    CONSTRAINT [PK_imagenesprograma] PRIMARY KEY CLUSTERED ([idimagenesprograma] ASC, [Activo] ASC)
);

