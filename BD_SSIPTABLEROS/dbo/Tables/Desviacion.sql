﻿CREATE TABLE [dbo].[Desviacion] (
    [IdIntervencion] INT           NOT NULL,
    [Activo]         VARCHAR (10)  NOT NULL,
    [IdDesviacion]   INT           NULL,
    [Desviacion]     VARCHAR (500) NULL,
    [IdEtapa]        INT           NULL,
    CONSTRAINT [PK_Desviacion_Seguimiento] PRIMARY KEY CLUSTERED ([IdIntervencion] ASC, [Activo] ASC),
    CONSTRAINT [FK_Desviacion_Etapas] FOREIGN KEY ([IdIntervencion], [Activo]) REFERENCES [dbo].[Etapas] ([IdIntervencion], [Activo]) ON DELETE CASCADE ON UPDATE CASCADE
);

