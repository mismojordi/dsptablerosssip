﻿CREATE PROCEDURE [dbo].[sp_Resumen]
@IdIntervencion int
WITH EXEC AS CALLER
AS
SELECT     dbo.intervencion.idIntervencion, dbo.intervencion.Activo, dbo.intervencion.NombrePozo, dbo.intervencion.Descripcion, dbo.intervencion.Cuota, 
                      dbo.intervencion.NombreEquipo, dbo.Resumen.RentaDiariaMd, dbo.intervencion.Semaforo, dbo.intervencion.Diferencia, dbo.intervencion.Porcentaje, 
                      dbo.intervencion.Avance, dbo.Resumen.RealDias, dbo.Resumen.RealPorciento, dbo.Resumen.NormalesSiop, dbo.Resumen.NptSiop, 
                      dbo.Resumen.PerdidaProdPotencialMd, dbo.Resumen.PerdidaCostoEquipoMd, dbo.intervencion.FechaInicio, dbo.intervencion.FechaTermino, 
                      dbo.Resumen.FechaInicioPrograma, dbo.Resumen.FechaTerminoPrograma, dbo.Resumen.EtapaActual, dbo.Resumen.OperacionActualUpa, 
                      dbo.Resumen.OperacionSiguienteUpa, dbo.intervencion.VpnProgramado, dbo.intervencion.VpnReal, dbo.intervencion.RiesgoProgramado, 
                      dbo.intervencion.RiesgoReal, dbo.Resumen.Objetivo
FROM         dbo.intervencion INNER JOIN
                      dbo.Resumen ON dbo.intervencion.idIntervencion = dbo.Resumen.IdIntervencion AND dbo.intervencion.Activo = dbo.Resumen.Activo
WHERE     (dbo.intervencion.idIntervencion = @IdIntervencion)
