﻿CREATE PROCEDURE [dbo].[sp_Intervenciones]
@Estatus varchar(20)
WITH EXEC AS CALLER
AS
-- Seleccion las intervenciones de acuerdo al estatus

IF @Estatus = 'En Proceso'
BEGIN
  SELECT     idIntervencion, Activo, NombrePozo, NombreEquipo, FechaInicio, FechaTermino, FechaTerminoProy, Semaforo, Diferencia, Porcentaje, Avance, VpnProgramado, 
                        VpnReal, RiesgoProgramado, RiesgoReal, Cuota, IdPozo, IdEquipo, Descripcion, Estatus, Act, Ordenamiento
  FROM         intervencion
  WHERE     (Estatus = @Estatus)
  ORDER BY Activo, Ordenamiento, FechaTermino
END

IF @Estatus = 'Concluido'
BEGIN
  SELECT     idIntervencion, Activo, NombrePozo, NombreEquipo, FechaInicio, FechaTermino, FechaTerminoProy, Semaforo, Diferencia, Porcentaje, Avance, VpnProgramado, 
                        VpnReal, RiesgoProgramado, RiesgoReal, Cuota, IdPozo, IdEquipo, Descripcion, Estatus, Act, Ordenamiento
  FROM         intervencion
  WHERE     (Estatus = @Estatus AND year(FechaTermino) = year(GETDATE()))
  ORDER BY Activo, Ordenamiento, FechaTermino
END

IF @Estatus = 'Todos'
BEGIN
  SELECT     idIntervencion, Activo, NombrePozo, NombreEquipo, FechaInicio, FechaTermino, FechaTerminoProy, Semaforo, Diferencia, Porcentaje, Avance, VpnProgramado, 
                        VpnReal, RiesgoProgramado, RiesgoReal, Cuota, IdPozo, IdEquipo, Descripcion, Estatus, Act, Ordenamiento
  FROM         intervencion
  WHERE     (year(FechaTermino) = year(GETDATE()))
  ORDER BY Activo, Ordenamiento, FechaTermino
END