﻿using System;
using System.Collections;
using System.Linq;
using System.Text;
using Persistencia.Entities;


namespace SsipTableros_Controller.General
{
    public class StateIntervenciones : IState
    {
        public EntIntervencion Record { get; set; }
        public IList RecordList { get; set; }

        public StateIntervenciones()
        {
            this.Record = new EntIntervencion();
        }
    }
}
