﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SsipTableros_Controller.General;
using Persistencia.Persistencias;
using Persistencia.Entities;

namespace SsipTableros_Controller.General
{
    public class CIntervenciones : IControl
    {
        private StateIntervenciones _state;
        public StateIntervenciones State
        {
            get { return _state; }
            set { _state = value; }
        }



        public CIntervenciones()
        {
            this.State = new StateIntervenciones();
        }

        public void SelectEntity()
        {
            try
            {
                PerIntervenciones PInter = new PerIntervenciones();
                State.Record = (EntIntervencion)PInter.Select(State.Record);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public void SelectAllEntity()
        {
         //   try
          //  {
                PerIntervenciones PInter = new PerIntervenciones();
                State.RecordList = PInter.SelectAll(State.Record);
          //  }
           // catch (Exception ex)
            //{
             //   throw new Exception(ex.Message);
            //}
        }
    }
}
