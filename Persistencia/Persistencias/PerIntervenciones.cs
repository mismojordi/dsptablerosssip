﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Reflection;
using System.Text;
using Persistencia.Entities;
using Bifurcacion.Switch;


namespace Persistencia.Persistencias
{
    public class PerIntervenciones
    {
        private static DataTable ToDataTable<T>(List<T> items)
        {
            DataTable dataTable = new DataTable(typeof(T).Name);

            //Get all the properties
            PropertyInfo[] Props = typeof(T).GetProperties(BindingFlags.Public | BindingFlags.Instance);
            foreach (PropertyInfo prop in Props)
            {
                //Setting column names as Property names
                dataTable.Columns.Add(prop.Name);
            }
            foreach (T item in items)
            {
                var values = new object[Props.Length];
                for (int i = 0; i < Props.Length; i++)
                {
                    //inserting property values to datatable rows
                    values[i] = Props[i].GetValue(item, null);
                }
                dataTable.Rows.Add(values);
            }
            //put a breakpoint here and check datatable
            return dataTable;
        }

        public IEntity Select(IEntity entity)
        {
            try
            {

                //instancia de los metodos de switcheo
                SwitchDspLocal metodos = new SwitchDspLocal();
                //datatable de result
                DataTable dtResult = new DataTable();
                //instancia de la entidad con la propiedad de estatus seteada
                EntIntervencion eInter = (EntIntervencion)entity;
                eInter.Estatus = "En Proceso";
                //llamar el metodo con true para DSP
                dtResult = metodos.IntervencionesMerge(eInter.Estatus, true);
                
                
                
                //llenar la entidad
                EntIntervencion Record = new EntIntervencion();
                foreach (DataRow dr in dtResult.Rows)
                {
                    Record.Activo = ValidateString.IsNull(dr, "Activo");
                    Record.NombrePozo = ValidateString.IsNull(dr, "NombrePozo");
                    Record.NombreEquipo = ValidateString.IsNull(dr, "NombreEquipo");
                    Record.FechaInicio = ValidateDateTime.IsNull(dr, "FechaInicio");
                    Record.FechaTermino = ValidateDateTime.IsNull(dr, "FechaTermino");
                    Record.FechaTerminoProy = ValidateDateTime.IsNull(dr, "FechaTerminoProy");
                    Record.Semaforo = ValidateString.IsNull(dr, "Semaforo");
                    Record.Diferencia = ValidateFloat.IsNull(dr, "Diferencia");
                    Record.Porcentaje = ValidateFloat.IsNull(dr, "Porcentaje");
                    Record.Avance = ValidateFloat.IsNull(dr, "Avance");
                    Record.VpnProgramado = ValidateFloat.IsNull(dr, "VpnProgramado");
                    Record.VpnReal = ValidateFloat.IsNull(dr, "VpnReal");
                    Record.RiesgoProgramado = ValidateString.IsNull(dr, "RiesgoProgramado");
                    Record.RiesgoReal = ValidateString.IsNull(dr, "RiesgoReal");
                    Record.Cuota = ValidateInt.IsNull(dr, "Cuota");
                    Record.IdPozo = ValidateInt.IsNull(dr, "IdPozo");
                    Record.IdEquipo = ValidateInt.IsNull(dr, "IdEquipo");
                    Record.Descripcion = ValidateString.IsNull(dr, "Descripcion");
                    Record.Estatus = ValidateString.IsNull(dr, "Estatus");
                    Record.Act = ValidateString.IsNull(dr, "Act");
                    Record.Ordenamiento = ValidateInt.IsNull(dr, "Ordenamiento");


                }

                return Record;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public IList SelectAll(IEntity entity)
        {
          //  try
            //{

                //instancia de los metodos de switcheo
                SwitchDspLocal metodos = new SwitchDspLocal();
                //datatable de result
                DataTable dtResult = new DataTable();                
                //instancia de la entidad con la propiedad de estatus seteada
                EntIntervencion eInter = (EntIntervencion)entity;
                //llamar el metodo con true para DSP
                dtResult = metodos.IntervencionesMerge(eInter.Estatus, true);
                

                EntIntervencion Record;
                 IList ListInter = new List<EntIntervencion>();
                foreach (DataRow dr in dtResult.Rows)
                {
                    
                    Record = new EntIntervencion();
                    Record.Activo = ValidateString.IsNull(dr, "Activo");
                    Record.NombrePozo = ValidateString.IsNull(dr, "NombrePozo");
                    Record.NombreEquipo = ValidateString.IsNull(dr, "NombreEquipo");
                    Record.FechaInicio = DateTime.Parse( dr["FechaInicio"].ToString()); // ValidateDateTime.IsNull(dr, "FechaInicio");
                    Record.FechaTermino = DateTime.Parse(dr["FechaInicio"].ToString());
                    Record.FechaTerminoProy = DateTime.Parse(dr["FechaInicio"].ToString());
                    Record.Semaforo = ValidateString.IsNull(dr, "Semaforo");
                    Record.Diferencia = ValidateFloat.IsNull(dr, "Diferencia");
                    Record.Porcentaje = ValidateFloat.IsNull(dr, "Porcentaje");
                    Record.Avance = ValidateFloat.IsNull(dr, "Avance");
                    Record.VpnProgramado = ValidateFloat.IsNull(dr, "VpnProgramado");
                    Record.VpnReal = ValidateFloat.IsNull(dr, "VpnReal");
                    Record.RiesgoProgramado = ValidateString.IsNull(dr, "RiesgoProgramado");
                    Record.RiesgoReal = ValidateString.IsNull(dr, "RiesgoReal");
                    Record.Cuota = ValidateInt.IsNull(dr, "Cuota");
                    Record.IdPozo = ValidateInt.IsNull(dr, "IdPozo");
                    Record.IdEquipo = ValidateInt.IsNull(dr, "IdEquipo");
                    Record.Descripcion = ValidateString.IsNull(dr, "Descripcion");
                    Record.Estatus = ValidateString.IsNull(dr, "Estatus");
                    Record.Act = ValidateString.IsNull(dr, "Act");
                    Record.Ordenamiento = ValidateInt.IsNull(dr, "Ordenamiento");
                    ListInter.Add(Record);


                }

                return ListInter;
            
            /*}
            
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
         */
        }
    }
}
