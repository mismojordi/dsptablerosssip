﻿using System;
using System.Collections;
using System.Linq;
using System.Text;
using Persistencia.Entities;

namespace Persistencia.Persistencias
{
    public interface IPersistencia
    {
        /// <summary>
        /// Metodo para insertar una entidad a persistencia.
        /// </summary>
        //void Insert(IEntity entity);
        /// <summary>
        /// Metodo para modificar una entidad en persistencia.
        /// </summary>
        //void Update(IEntity entity);
        /// <summary>
        /// Metodo para eliminar una entidad de persistencia.
        /// </summary>
        //void Delete(IEntity entity);
        /// <summary>
        /// Metodo para obtener una entidad.
        /// </summary>
        IEntity Select(IEntity entity);
        /// <summary>
        /// Metodo para obtener todas las entidades relacionadas a las intervenciones En Proceso.
        /// </summary>
        IList SelectAll(IEntity entity);
    }
}
