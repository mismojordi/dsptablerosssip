﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Persistencia.Entities
{
    //[Serializable]
    public class EntSemaforoVpn
    {
        //Propiedades
            public string Semaforo { get; set; }
            public double Valor { get; set; }

            //Constructor
            public EntSemaforoVpn()
            {
                Semaforo = string.Empty;
                Valor = 0;
            }
    }
}
