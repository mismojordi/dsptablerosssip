﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace Persistencia.Entities
{
    static public class ValidateInt
    {
        static public int IsNull(DataRow dr, string field)
        {
            try
            {
                return dr[field].Equals(DBNull.Value) ? 0 : int.Parse(dr[field].ToString());
            }
            catch (Exception ex)
            {
                throw new Exception("ValidateInt.IsNull -> " + ex.Message);
            }
        }
    }
}
