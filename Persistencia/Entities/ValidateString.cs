﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace Persistencia.Entities
{
    static public class ValidateString
    {
        static public string IsNull(DataRow dr, string field)
        {
            try
            {
                return dr[field].Equals(DBNull.Value) ? string.Empty : dr[field].ToString();
            }
            catch (Exception ex)
            {
                throw new Exception("ValidateString.IsNull -> " + ex.Message);
            }
        }



        static public string IsNull(string dr, string field)
        {
            try
            {
                return dr.Equals(DBNull.Value) ? string.Empty : dr.ToString();
            }
            catch (Exception ex)
            {
                throw new Exception("ValidateString.IsNull -> " + ex.Message);
            }
        }


    }

}

    
