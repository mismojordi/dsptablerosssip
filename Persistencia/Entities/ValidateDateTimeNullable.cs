﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace Persistencia.Entities
{
    static public class ValidateDateTimeNullable
    {
        static public DateTime? IsNull(DataRow dr, string field)
        {
            try
            {
                return dr[field].Equals(DBNull.Value) ? null : (DateTime?)DateTime.Parse(dr[field].ToString());
            }
            catch (Exception ex)
            {
                throw new Exception("ValidateDateTime.IsNull -> " + ex.Message);
            }
        }
    }
}
