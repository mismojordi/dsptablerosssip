﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace Persistencia.Entities
{
    static public class ValidateFloat
    {
        static public float IsNull(DataRow dr, string field)
        {
            try
            {
                return dr[field].Equals(DBNull.Value) ? 0 : float.Parse(dr[field].ToString());
            }
            catch (Exception ex)
            {
                throw new Exception("ValidateInt.IsNull -> " + ex.Message);
            }
        }
    }
}
