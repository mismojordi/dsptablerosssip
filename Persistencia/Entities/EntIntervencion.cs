﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Persistencia.Entities
{
    [Serializable]
    public class EntIntervencion : IEntity
    {
        #region Properties

        public string Activo { get; set; }

        public string NombrePozo { get; set; }

        public string NombreEquipo { get; set; }

        public DateTime? FechaInicio { get; set; }

        public DateTime? FechaTermino { get; set; }

        public DateTime? FechaTerminoProy { get; set; }

        public string Semaforo { get; set; }

        public float Diferencia { get; set; }

        public double Porcentaje { get; set; }

        public float Avance { get; set; }

        public float VpnProgramado { get; set; }

        public float VpnReal { get; set; }

        public string RiesgoProgramado { get; set; }

        public string RiesgoReal { get; set; }

        public int? Cuota { get; set; }

        public int IdPozo { get; set; }

        public int IdEquipo { get; set; }

        public string Descripcion { get; set; }

        public string Estatus { get; set; }

        public string Act { get; set; }

        public int? Ordenamiento { get; set; }
        #endregion


       #region Constructor
        public EntIntervencion()
        {
            this.Activo = "Abkatun";
            this.NombrePozo = string.Empty;
            this.NombreEquipo = string.Empty;
            this.FechaInicio = new DateTime(1900, 1, 1);
            this.FechaTermino = new DateTime(1900, 1, 1);
            this.FechaTerminoProy = new DateTime(1900, 1, 1);
            this.Semaforo = string.Empty;
            this.Diferencia = 0;
            this.Porcentaje = 0;
            this.Avance = 0;
            this.VpnProgramado = 0;
            this.VpnReal = 0;
            this.RiesgoProgramado = string.Empty;
            this.RiesgoReal = string.Empty;
            this.Cuota = 0;
            this.IdPozo = 0;
            this.IdEquipo = 0;
            this.Descripcion = string.Empty;
            this.Estatus = string.Empty;
            this.Act = string.Empty;
            this.Ordenamiento = 0;
        }
        #endregion
    }
}
