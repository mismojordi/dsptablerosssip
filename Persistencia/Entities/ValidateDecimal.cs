﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace Persistencia.Entities
{
    static public class ValidateDecimal
    {
        static public decimal? IsNull(DataRow dr, string field)
        {
            try
            {
                return dr[field].Equals(DBNull.Value) ? null : (decimal?)Convert.ToDecimal(dr[field]);
            }
            catch (Exception ex)
            {
                throw new Exception("ValidateDecimal.IsNull -> " + ex.Message);
            }
        }
    }
}
