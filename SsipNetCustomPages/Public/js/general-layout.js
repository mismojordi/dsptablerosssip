var SizeScreen = function() {
      var sizeScreen = Array();
      var myWidth = 0, myHeight = 0;
      if( typeof( window.innerWidth ) == 'number' ) {
        //Non-IE
        myWidth = window.innerWidth;
        myHeight = window.innerHeight;
      } else if( document.documentElement && ( document.documentElement.clientWidth || document.documentElement.clientHeight ) ) {
        //IE 6+ in 'standards compliant mode'
        myWidth = document.documentElement.clientWidth;
        myHeight = document.documentElement.clientHeight;
      } else if( document.body && ( document.body.clientWidth || document.body.clientHeight ) ) {
        //IE 4 compatible
        myWidth = document.body.clientWidth;
        myHeight = document.body.clientHeight;
      }
      sizeScreen['width'] = myWidth;
      sizeScreen['height'] = myHeight;
      return sizeScreen;
}();

//Soporte para 1024
if(SizeScreen.width >= 1020 && SizeScreen.width <= 1030){
    document.getElementById('css-soport').href = "../Public/css/general-layout-1024.css";
}