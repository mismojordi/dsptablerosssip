﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Bifurcacion.Switch;
using System.Data;

namespace SsipTableros.Modulos.TableroGlobal
{
    public partial class EstadoMecanico : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

            if (!IsPostBack)
            {

                //llenar la lista de fechas de estados mecanicos
                SwitchDspLocal metodos = new SwitchDspLocal();
                //TODO: obtener los argumentos
                DataTable dt = metodos.ConsultarImagenes(134824, "Abkatun", "estadomecanico");
                //Vincular
                lstEstadosMecanicos.DataSource = dt;
                lstEstadosMecanicos.DataValueField = "Ruta";
                lstEstadosMecanicos.DataTextField = "Fecha";
                lstEstadosMecanicos.DataBind();
            }

        }

        protected void lstEstadosMecanicos_SelectedIndexChanged(object sender, EventArgs e)
        {
            TextBox1.Text = lstEstadosMecanicos.SelectedValue;
          //  System.IO.DirectoryInfo rootDirN = new System.IO.DirectoryInfo("\\\\192.168.168.89\\compartido\\EstadosMecanicos\\134824_EstadosMecanicos_2014_05_23_06_48.pdf");
            ViewState["Ruta"] =  "/Public/" + lstEstadosMecanicos.SelectedValue;
        }

       
    }
}