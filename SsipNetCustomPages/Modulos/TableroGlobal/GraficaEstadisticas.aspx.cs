﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Bifurcacion.Switch;
using System.Data;
using System.Text.RegularExpressions;
using System.Web.Script.Serialization;
using System.Text;


namespace SsipTableros.Modulos.TableroGlobal
{
    public partial class GraficaEstadisticas : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //TODO RECIBIR POR REQUEST la fecha inicio, fecha fin, activo, idintervencion,
            
            //TODO: Leer el IdIntervencion
            int IdIntervencion = 134824;
             //TODO: Leer el Activo   
            
            string  Activo = "Abkatun";
            //instancia de los metodos de acceso a datos local y dsp
            SwitchDspLocal metodos = new SwitchDspLocal();
            //obtener la informacion de la intervencion para conseguir el nombre del pozo
            DataTable InfoIntervencion = metodos.InfoIntervencion(IdIntervencion, Activo);
            //obtener el nombre del pozo
            string Pozo = InfoIntervencion.Rows[0]["NombrePozo"].ToString();
            //obtener la actividad com perforacion, etc.
            string Intervencion = InfoIntervencion.Rows[0]["Actividad"].ToString();
            //obtener fechar min y max de las estaditicas
            DataTable FechasMinMax = metodos.FechasMinMaxEstadisticas(IdIntervencion, Activo);
            DateTime FechaMin =  DateTime.Parse(FechasMinMax.Rows[0]["FechaMin"].ToString());
            DateTime FechaMax = DateTime.Parse(FechasMinMax.Rows[0]["FechaMax"].ToString());
            //crear el subtitulo de la grafica, pozo + rango de fechas
            string PozoRangoFechas = "Pozo " + Pozo +  " (del " + FechaMin.ToShortDateString() + " al " + FechaMax.ToShortDateString() + ")";
            ViewState["Pozo"] = PozoRangoFechas;
            //oBtener los NTPS
            DataTable Npts = metodos.EstadisticasDiasTotales(IdIntervencion, Activo, FechaMin, FechaMax);
            //obtener cada tipo
            float TotalNormales = (float)Npts.Rows[0]["OPN"];
            float TotalProblemas = (float)Npts.Rows[0]["OPC"];
            float TotalAdicionales = (float)Npts.Rows[0]["OPA"];
            float TotalEsperas = (float)Npts.Rows[0]["ESP"];
            //enviar a la vista
            #region grafica_columnas
            //generamos el codigo para el data de la grafica de columnas
              string codigoColumnas = "";
            	//Operaciones Normales 
              if (TotalNormales > 0)
              {
                  codigoColumnas += "{";
                  codigoColumnas += "type: 'column',";
                  codigoColumnas += "name: 'Operaciones Normales',";
                  codigoColumnas += "shadow: false,";
                  codigoColumnas += "data:[";
                  codigoColumnas += "{y: " +  TotalNormales + "}";

                  codigoColumnas += "]";
                  codigoColumnas += "}, ";

              }

              //Operaciones con Problemas 
              if (TotalProblemas > 0)
              {
                  codigoColumnas += "{";
                  codigoColumnas += "type: 'column',";
                  codigoColumnas += "name: 'Operaciones con Problemas',";
                  codigoColumnas += "shadow: false,";
                  codigoColumnas += "data:[";
                  codigoColumnas += "{y: " + TotalProblemas + "}";

                  codigoColumnas += "]";
                  codigoColumnas += "}, ";

              }


              //Operaciones  Adicionales 
              if (TotalAdicionales > 0)
              {
                  codigoColumnas += "{";
                  codigoColumnas += "type: 'column',";
                  codigoColumnas += "name: 'Operaciones Adicionales',";
                  codigoColumnas += "shadow: false,";
                  codigoColumnas += "data:[";
                  codigoColumnas += "{y: " + TotalAdicionales + "}";

                  codigoColumnas += "]";
                  codigoColumnas += "}, ";

              }

              //Esperas
              if (TotalEsperas > 0)
              {
                  codigoColumnas += "{";
                  codigoColumnas += "type: 'column',";
                  codigoColumnas += "name: 'Operaciones Adicionales',";
                  codigoColumnas += "shadow: false,";
                  codigoColumnas += "data:[";
                  codigoColumnas += "{y: " + TotalEsperas + "}";

                  codigoColumnas += "]";
                  codigoColumnas += "}, ";

              }

            //eliminar caracteres adicionales           
              if (codigoColumnas.Length > 0)
                  codigoColumnas = codigoColumnas.Substring(0, codigoColumnas.Length - 2);
            	
            	//Enviar el codigo
                ViewState["CodigoColumnas"] = codigoColumnas;
            #endregion

            #region grafica_pastel
            //obtener el total
                float TotalDias = TotalNormales + TotalProblemas + TotalEsperas + TotalAdicionales;
            //calculamos los porcentajes
                float PorcientoNormales = TotalNormales / TotalDias * 100;
                float PorcientoProblemas = TotalProblemas / TotalDias * 100;
                float PorcientoAdicionales = TotalEsperas / TotalDias * 100;
                float PorcientoEsperas = TotalAdicionales / TotalDias * 100;
                float TotalPorcientos = (float) 100;
                float[] Porcientos = new float[5] { PorcientoNormales, PorcientoProblemas, PorcientoAdicionales, PorcientoEsperas, TotalPorcientos };
            //generamos el codigo para la grafica
                              
                       	
                       	string CodigoPastel = "";                       	
                       	CodigoPastel += (PorcientoNormales>0)    ?  "['Operaciones Normales', " +PorcientoNormales+ "], "       : "";
                       	CodigoPastel += (PorcientoProblemas>0)   ?  "['Operaciones con Problemas', " +PorcientoProblemas+ "], " : "";
                        CodigoPastel += (PorcientoAdicionales > 0) ? "['Operaciones Adicionales', " + PorcientoAdicionales + "], " : "";
                        CodigoPastel += (PorcientoEsperas > 0) ? "['Esperas', " + PorcientoEsperas + "], " : "";
                       	
              //enviamos a la vista
                        ViewState["CodigoPastel"] = CodigoPastel;
                       	
                   

            #endregion

            #region grafica_areas
            //obtener las operaciones para la grafica de area
                        DataTable NormalesArea = metodos.HorasDiasGrupoActividades(IdIntervencion, Activo, "OPN", FechaMin, FechaMax);
                        DataTable ProblemasArea = metodos.HorasDiasGrupoActividades(IdIntervencion, Activo, "OPC", FechaMin, FechaMax);
                        DataTable AdicionalesArea = metodos.HorasDiasGrupoActividades(IdIntervencion, Activo, "OPA", FechaMin, FechaMax);
                        DataTable EsperasArea = metodos.HorasDiasGrupoActividades(IdIntervencion, Activo, "ESP", FechaMin, FechaMax);
            //generar el codigo para la grafica, normales
             string CodigoAreas = "";
             if(TotalNormales > 0)
            {
                CodigoAreas = "{";                                   
                CodigoAreas += " type : 'area', ";
				CodigoAreas+= "name: 'Operaciones Normales',";
				CodigoAreas+= "data: [";
				foreach(DataRow datosFecha in NormalesArea.Rows) 
				{
					CodigoAreas += "{";
					CodigoAreas += "x: Date.UTC("+   FechaUTC(datosFecha["FechaInicio"].ToString())  +"),";
					CodigoAreas += "y:"+ datosFecha["TotalHoras"];
					CodigoAreas += "},";
					
				}
				 //quitar la ultima coma
				CodigoAreas = CodigoAreas.Substring(0,CodigoAreas.Length-1);            
				    	      									
				CodigoAreas += " ]";
                CodigoAreas += " }, ";

                }
             //generar el codigo para la grafica, problemas           
             if (TotalProblemas > 0)
             {
                 CodigoAreas += "{";    
                 CodigoAreas += " type : 'area', ";
                 CodigoAreas += "name: 'Operaciones con Problemas',";
                 CodigoAreas += "data: [";
                 foreach (DataRow datosFecha in ProblemasArea.Rows)
                 {
                     CodigoAreas += "{";
                     CodigoAreas += "x: Date.UTC(" + FechaUTC(datosFecha["FechaInicio"].ToString()) + "),";
                     CodigoAreas += "y:" + datosFecha["TotalHoras"];
                     CodigoAreas += "},";

                 }
                 //quitar la ultima coma
                 CodigoAreas = CodigoAreas.Substring(0, CodigoAreas.Length - 1);
                 CodigoAreas += " ]";
                 CodigoAreas += " }, ";

             }
             //generar el codigo para la grafica, Adicionales           
             if (TotalAdicionales > 0)
             {
                 CodigoAreas += "{";
                 CodigoAreas += " type : 'area', ";
                 CodigoAreas += "name: 'Operaciones Adicionales',";
                 CodigoAreas += "data: [";
                 foreach (DataRow datosFecha in AdicionalesArea.Rows)
                 {
                     CodigoAreas += "{";
                     CodigoAreas += "x: Date.UTC(" + FechaUTC(datosFecha["FechaInicio"].ToString()) + "),";
                     CodigoAreas += "y:" + datosFecha["TotalHoras"];
                     CodigoAreas += "},";

                 }
                 //quitar la ultima coma
                 CodigoAreas = CodigoAreas.Substring(0, CodigoAreas.Length - 1);
                 CodigoAreas += " ]";
                 CodigoAreas += " }, ";

             }

             //generar el codigo para la grafica, Esperas           
             if (TotalEsperas > 0)
             {
                 CodigoAreas += "{";
                 CodigoAreas += " type : 'area', ";
                 CodigoAreas += "name: 'Esperas',";
                 CodigoAreas += "data: [";
                 foreach (DataRow datosFecha in EsperasArea.Rows)
                 {
                     CodigoAreas += "{";
                     CodigoAreas += "x: Date.UTC(" + FechaUTC(datosFecha["FechaInicio"].ToString()) + "),";
                     CodigoAreas += "y:" + datosFecha["TotalHoras"];
                     CodigoAreas += "},";

                 }
                 //quitar la ultima coma
                 CodigoAreas = CodigoAreas.Substring(0, CodigoAreas.Length - 1);
                 CodigoAreas += " ]";
                 CodigoAreas += " }, ";

             }
             //enviamos a la vista
             ViewState["CodigoAreas"] = CodigoAreas;


         #endregion


            #region graficas_normales
             #region columnas
             //recuperar la operaciones Normales
             string[] operaciones= new string[4] {"OPN", "OPC", "OPA", "ESP"};
             string[] actividades = new string[4] { "Operaciones Normales", "Operaciones con Problemas", "OPeraciones Adicionales", "Esperas" };
             string[] containercolumnas = new string[4] { "containerOPN", "containerOPC", "containerOPA", "containerESP" };
             string[] containerpastel = new string[4] { "containerPasteles0", "containerPasteles1", "containerPasteles2", "containerPasteles3" };
             string[] containerarea = new string[4] { "containerArea0", "containerArea1", "containerArea2", "containerArea3" };
             string[] GraficaColumnas = new string[4];
             string[] GraficaPastel = new string[4];
             string[] GraficaArea = new string[4];
             float[] Totales = new float[5] { TotalNormales, TotalProblemas, TotalAdicionales, TotalEsperas,TotalDias };
             TablaOperaciones Tablas;
            List<TablaOperaciones> ListaTablas = new List<TablaOperaciones>();        


             for (int i = 0; i <= 3; i++)
             {

                 DataTable dt = metodos.DuracionGrupoActividades(IdIntervencion, Activo, operaciones[i], FechaMin, FechaMax);
                 
                 string CodigoColumnas = "";
                 string CodigoCircular = "";
                 string Areas = "";
                 if (dt.Rows.Count > 0)
                 {
                                          
                     foreach (DataRow dr in dt.Rows)
                     {
                         //Obtener del datarow
                         string Nombre = dr["Nombre"].ToString();
                         //calcular duracion dias
                         decimal Dias = (decimal)dr["TotalHoras"];
                         Dias = Math.Round(Dias / 24, 2);
                         //codificar y serrializar para acenotos y escapes
                         Nombre = AjustarAcentos(Nombre);
                         //formar el codigo de la gráfica de columnas
                         CodigoColumnas += "{";
                         CodigoColumnas += "type: 'column',";
                         CodigoColumnas += "name: '" + Nombre + "',";
                         CodigoColumnas += "shadow: false,";
                         CodigoColumnas += "data:[";
                         CodigoColumnas += "{y: " + Dias + "}";
                         CodigoColumnas += "]";
                         CodigoColumnas += "}, ";
                         //formar el codigo de la gráfica de pastel
                         Decimal Porciento = (decimal) Totales[i] * Dias / 100;
                         Porciento = Math.Round(Porciento, 2);
                         CodigoCircular += "['" + Nombre + "', " + Porciento + "], ";

                         //crear el registro
                         Tablas.Nombre = Nombre;
                         Tablas.Dias = Dias;
                         Tablas.Porciento = Porciento;
                         Tablas.Operacion = operaciones[i];
                         //agregar a la lista
                         ListaTablas.Add(Tablas);
                     }
                     //Eliminar la ultima coma ,
                     CodigoColumnas = CodigoColumnas.Substring(0, CodigoColumnas.Length - 2);
                     //enviar a la vista.
                     ViewState["grafica"] = CodigoColumnas;
                     ViewState["pastel"] = CodigoCircular;

             #endregion

                     #region areas
                     //recuperar las opeaciones por horas
                     DataTable dth = metodos.HorasDiasGrupoActividades(IdIntervencion, Activo, operaciones[i], FechaMin, FechaMax);
                     
                     //formar el codigo para la grafica de areas
                     foreach (DataRow dr in dth.Rows)
                     {
                         Areas += "{";
                         Areas += "x: Date.UTC(" + FechaUTC(dr["FechaInicio"].ToString()) + "),";
                         Areas += "y:" + dr["TotalHoras"];
                         Areas += "},";
                     }
                     Areas = Areas.Substring(0, Areas.Length - 1);


                     ViewState["Areas"] = Areas;



                 }//fin del if
              
                 //asignar al array
                 GraficaColumnas[i] = CodigoColumnas;
                 GraficaPastel[i] = CodigoCircular;
                 GraficaArea[i] = Areas;
                                 

             } // fin del for

            //retornar los array a la vista
             ViewState["ArrayColumnas"] = GraficaColumnas;
             ViewState["ArrayPastel"] = GraficaPastel;
             ViewState["ArrayAreas"] = GraficaArea;
             ViewState["operaciones"] = operaciones;
             ViewState["operaciones"] = operaciones;
             ViewState["actividades"] = actividades;
             ViewState["containercolumnas"] = containercolumnas;
             ViewState["containerpastel"] = containerpastel;
             ViewState["containerarea"] = containerarea;
             ViewState["ListaTablas"] = ListaTablas;
             ViewState["Intervencion"] = Intervencion;
             ViewState["Totales"] = Totales;
             ViewState["Porcientos"] = Porcientos;

            #endregion
            #endregion
        }

        //devolvemos la fecha separada por comas ,
        string FechaUTC(string Fecha)
        {
            DateTime f = DateTime.Parse(Fecha);
            return f.Year.ToString() + "," + f.Month.ToString() + "," + f.Day.ToString();


        }


        //ajustas acentos
        string AjustarAcentos(string Nombre)
        {
                       
            //cotidificar de latin a utf8
            Encoding iso = Encoding.GetEncoding("ISO-8859-1");
            Encoding utf8 = Encoding.UTF8;
            Nombre = utf8.GetString(iso.GetBytes(Nombre));
            Nombre = new JavaScriptSerializer().Serialize(Nombre);
            //Eliminar la comillas JSON
            Nombre = Nombre.Substring(1, Nombre.Length - 1);
            Nombre = Nombre.Substring(0, Nombre.Length - 1);
            return Nombre;
        }

       public struct TablaOperaciones
            {
              public string Nombre;
              public Decimal Dias;
              public Decimal Porciento;
              public string Operacion;
            
            }

    }
}