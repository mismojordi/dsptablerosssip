﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SsipTableros_Controller.General;
using Persistencia.Entities;
using System.Linq;
using System.Data.Linq;

namespace SsipTableros.Modulos.TableroGlobal
{
    public partial class Tablero : System.Web.UI.Page
    {
        #region Variables Globales
        //Variables Global
        CIntervenciones LIntervenciones = new CIntervenciones();
        int count = 0;

        #endregion

        #region Page Load
        protected void Page_Load(object sender, EventArgs e)
        {
            //Validar Usuario
            SetOptionsMenu();
            ExtraerInfo();
        }

        #endregion

        #region Fill Tablas
        protected void ExtraerInfo()
        {
         //   try
           // {
                string Estatus = Request.QueryString["filtro"];
                FillIntervencion(Estatus);
                
            //}
          //  catch (Exception ex)
            //{
           //     ScriptManager.RegisterClientScriptBlock(this.Page, Page.GetType(), "Error...", "alert('" + ex.Message + "');", true);
            //}
        }

        private void FillIntervencion(string inter)
        {
          //  try
           // {
                if (inter != string.Empty && inter != null)
                {
                    FillInter(inter);
                }
                else
                {
                    FillInter("En Proceso");
                }
           // }
            //catch (Exception ex)
            //{
              //  ScriptManager.RegisterClientScriptBlock(this.Page, Page.GetType(), "Error...", "alert('" + ex.Message + "');", true);
            //}
        }

        #endregion

        #region Tablas

        #region Llenado de Tablas
        private void FillInter(string Estatus)
        {
            //Llenado de datos de Intervenciones en PROCESO
            //Se extrae la lista de elementos desde la BD
            LIntervenciones.State.Record.Estatus = Estatus;

            LIntervenciones.SelectAllEntity();
            //Se asigna la lista de elementos
            IList<EntIntervencion> InterEnProceso = (IList<EntIntervencion>)LIntervenciones.State.RecordList;
            IList<EntIntervencion> LInterTipo = (IList<EntIntervencion>)LIntervenciones.State.RecordList;

            //Llenamos los datos con las intervenciones para Perforación
            LInterTipo = InterEnProceso.Where(X => X.Descripcion == "PERFORACIÓN").ToList();
            RIntervencion.DataSource = LInterTipo;
            RIntervencion.DataBind();

            //Llenamos los datos con las intervenciones para Terminacion
            count = 0;
            LInterTipo = InterEnProceso.Where(X => X.Descripcion == "TERMINACIÓN").ToList();
            RTerminacion.DataSource = LInterTipo;
            RTerminacion.DataBind();

            //Llenamos los datos con las intervenciones para Reparación Mayor
            count = 0;
            LInterTipo = InterEnProceso.Where(X => X.Descripcion == "REPARACIÓN MAYOR").ToList();
            RReparacionMa.DataSource = LInterTipo;
            RReparacionMa.DataBind();

            //Llenamos los datos con las intervenciones para Reparación Menor
            count = 0;
            LInterTipo = InterEnProceso.Where(X => X.Descripcion == "REPARACIÓN MENOR").ToList();
            RReparacionMe.DataSource = LInterTipo;
            RReparacionMe.DataBind();

            //Llenamos los datos con las intervenciones para Estimulación
            count = 0;
            LInterTipo = InterEnProceso.Where(X => X.Descripcion == "ESTIMULACIÓN").ToList();
            REstimulacion.DataSource = LInterTipo;
            REstimulacion.DataBind();
        }
        #endregion

        #region Repetear Perforación
        protected void RIntervencion_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                EntIntervencion Inter = (EntIntervencion)e.Item.DataItem;
                EntSemaforoVpn sVpn = new EntSemaforoVpn();

                count++;

                Label FechaIni = (Label)e.Item.FindControl("lbFechaInicio");
                Label FechaFin = (Label)e.Item.FindControl("lbFechaFin");
                Label FechaFinP = (Label)e.Item.FindControl("lbFechaFinP");
                Label idReg = (Label)e.Item.FindControl("idReg");
                Image ImgSema = (Image)e.Item.FindControl("imgSemaforo");
                Image ImgVpn = (Image)e.Item.FindControl("imgVpn");
                Label lbVpn = (Label)e.Item.FindControl("lbVpn");
                Image ImgRProg = (Image)e.Item.FindControl("imgRProg");
                Image ImgRReal = (Image)e.Item.FindControl("imgRReal");

                idReg.Text = count.ToString();
                FechaIni.Text = (DateTime.Parse(FechaIni.Text)).ToShortDateString();
                FechaFin.Text = (DateTime.Parse(FechaFin.Text)).ToShortDateString();
                FechaFinP.Text = (DateTime.Parse(FechaFinP.Text)).ToShortDateString();
                ImgSema.ImageUrl = getSemaforo(Inter.Semaforo);

                //Obtnemos los Datos para Vpn
                sVpn = (SemaforoDesviacionVpn(Inter.VpnProgramado, Inter.VpnReal));
                ImgVpn.ImageUrl = getSemaforo(sVpn.Semaforo);
                lbVpn.Text = sVpn.Valor.ToString();

                //Obtenemos los Datos para Riesgos
                ImgRProg.ImageUrl = getSemaforo(getStringColor(ComportamientoRiesgo(Inter.RiesgoProgramado, Inter.RiesgoReal)));


            }
        }
        #endregion

        #region Repetear de Terminacion

        protected void RTerminacion_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                EntIntervencion Inter = (EntIntervencion)e.Item.DataItem;
                EntSemaforoVpn sVpn = new EntSemaforoVpn();

                count++;

                Label FechaIni = (Label)e.Item.FindControl("lbFechaInicio");
                Label FechaFin = (Label)e.Item.FindControl("lbFechaFin");
                Label FechaFinP = (Label)e.Item.FindControl("lbFechaFinP");
                Label idReg = (Label)e.Item.FindControl("idReg");
                Image ImgSema = (Image)e.Item.FindControl("imgSemaforo");
                Image ImgVpn = (Image)e.Item.FindControl("imgVpn");
                Label lbVpn = (Label)e.Item.FindControl("lbVpn");
                Image ImgRProg = (Image)e.Item.FindControl("imgRProg");
                Image ImgRReal = (Image)e.Item.FindControl("imgRReal");

                idReg.Text = count.ToString();
                FechaIni.Text = (DateTime.Parse(FechaIni.Text)).ToShortDateString();
                FechaFin.Text = (DateTime.Parse(FechaFin.Text)).ToShortDateString();
                FechaFinP.Text = (DateTime.Parse(FechaFinP.Text)).ToShortDateString();
                ImgSema.ImageUrl = getSemaforo(Inter.Semaforo);

                //Obtnemos los Datos para Vpn
                sVpn = (SemaforoDesviacionVpn(Inter.VpnProgramado, Inter.VpnReal));
                ImgVpn.ImageUrl = getSemaforo(sVpn.Semaforo);
                lbVpn.Text = sVpn.Valor.ToString();

                //Obtenemos los Datos para Riesgos
                ImgRProg.ImageUrl = getSemaforo(getStringColor(ComportamientoRiesgo(Inter.RiesgoProgramado, Inter.RiesgoReal)));


            }
        }

        #endregion

        #region Repeater de Reparación Mayor
        protected void RReparacionMa_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                EntIntervencion Inter = (EntIntervencion)e.Item.DataItem;
                EntSemaforoVpn sVpn = new EntSemaforoVpn();

                count++;

                Label FechaIni = (Label)e.Item.FindControl("lbFechaInicio");
                Label FechaFin = (Label)e.Item.FindControl("lbFechaFin");
                Label FechaFinP = (Label)e.Item.FindControl("lbFechaFinP");
                Label idReg = (Label)e.Item.FindControl("idReg");
                Image ImgSema = (Image)e.Item.FindControl("imgSemaforo");
                Image ImgVpn = (Image)e.Item.FindControl("imgVpn");
                Label lbVpn = (Label)e.Item.FindControl("lbVpn");
                Image ImgRProg = (Image)e.Item.FindControl("imgRProg");
                Image ImgRReal = (Image)e.Item.FindControl("imgRReal");

                idReg.Text = count.ToString();
                FechaIni.Text = (DateTime.Parse(FechaIni.Text)).ToShortDateString();
                FechaFin.Text = (DateTime.Parse(FechaFin.Text)).ToShortDateString();
                FechaFinP.Text = (DateTime.Parse(FechaFinP.Text)).ToShortDateString();
                ImgSema.ImageUrl = getSemaforo(Inter.Semaforo);

                //Obtnemos los Datos para Vpn
                sVpn = (SemaforoDesviacionVpn(Inter.VpnProgramado, Inter.VpnReal));
                ImgVpn.ImageUrl = getSemaforo(sVpn.Semaforo);
                lbVpn.Text = sVpn.Valor.ToString();

                //Obtenemos los Datos para Riesgos
                ImgRProg.ImageUrl = getSemaforo(getStringColor(ComportamientoRiesgo(Inter.RiesgoProgramado, Inter.RiesgoReal)));


            }
        }
        #endregion

        #region Repeater de Repación Menor
        protected void RReparacionMe_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                EntIntervencion Inter = (EntIntervencion)e.Item.DataItem;
                EntSemaforoVpn sVpn = new EntSemaforoVpn();

                count++;

                Label FechaIni = (Label)e.Item.FindControl("lbFechaInicio");
                Label FechaFin = (Label)e.Item.FindControl("lbFechaFin");
                Label FechaFinP = (Label)e.Item.FindControl("lbFechaFinP");
                Label idReg = (Label)e.Item.FindControl("idReg");
                Image ImgSema = (Image)e.Item.FindControl("imgSemaforo");
                Image ImgVpn = (Image)e.Item.FindControl("imgVpn");
                Label lbVpn = (Label)e.Item.FindControl("lbVpn");
                Image ImgRProg = (Image)e.Item.FindControl("imgRProg");
                Image ImgRReal = (Image)e.Item.FindControl("imgRReal");

                idReg.Text = count.ToString();
                FechaIni.Text = (DateTime.Parse(FechaIni.Text)).ToShortDateString();
                FechaFin.Text = (DateTime.Parse(FechaFin.Text)).ToShortDateString();
                FechaFinP.Text = (DateTime.Parse(FechaFinP.Text)).ToShortDateString();
                ImgSema.ImageUrl = getSemaforo(Inter.Semaforo);

                //Obtnemos los Datos para Vpn
                sVpn = (SemaforoDesviacionVpn(Inter.VpnProgramado, Inter.VpnReal));
                ImgVpn.ImageUrl = getSemaforo(sVpn.Semaforo);
                lbVpn.Text = sVpn.Valor.ToString();

                //Obtenemos los Datos para Riesgos
                ImgRProg.ImageUrl = getSemaforo(getStringColor(ComportamientoRiesgo(Inter.RiesgoProgramado, Inter.RiesgoReal)));


            }
        }
        #endregion

        #region Repeater Estimulación
        protected void REstimulacion_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                EntIntervencion Inter = (EntIntervencion)e.Item.DataItem;
                EntSemaforoVpn sVpn = new EntSemaforoVpn();

                count++;

                Label FechaIni = (Label)e.Item.FindControl("lbFechaInicio");
                Label FechaFin = (Label)e.Item.FindControl("lbFechaFin");
                Label FechaFinP = (Label)e.Item.FindControl("lbFechaFinP");
                Label idReg = (Label)e.Item.FindControl("idReg");
                Image ImgSema = (Image)e.Item.FindControl("imgSemaforo");
                Image ImgVpn = (Image)e.Item.FindControl("imgVpn");
                Label lbVpn = (Label)e.Item.FindControl("lbVpn");
                Image ImgRProg = (Image)e.Item.FindControl("imgRProg");
                Image ImgRReal = (Image)e.Item.FindControl("imgRReal");

                idReg.Text = count.ToString();
                FechaIni.Text = (DateTime.Parse(FechaIni.Text)).ToShortDateString();
                FechaFin.Text = (DateTime.Parse(FechaFin.Text)).ToShortDateString();
                FechaFinP.Text = (DateTime.Parse(FechaFinP.Text)).ToShortDateString();
                ImgSema.ImageUrl = getSemaforo(Inter.Semaforo);

                //Obtnemos los Datos para Vpn
                sVpn = (SemaforoDesviacionVpn(Inter.VpnProgramado, Inter.VpnReal));
                ImgVpn.ImageUrl = getSemaforo(sVpn.Semaforo);
                lbVpn.Text = sVpn.Valor.ToString();

                //Obtenemos los Datos para Riesgos
                ImgRProg.ImageUrl = getSemaforo(getStringColor(ComportamientoRiesgo(Inter.RiesgoProgramado, Inter.RiesgoReal)));


            }
        }
        #endregion

        #endregion

        #region Metodos

        private void SetOptionsMenu()
        {
            string Estatus = Request.QueryString["filtro"];
            if (Estatus != null)
            {
                liEjecucion.Attributes["class"] = (Estatus == "En Proceso") ? "active" : string.Empty;
                liConcluida.Attributes["class"] = (Estatus == "Concluido") ? "active" : string.Empty;
            }
        }

        private string getStringColor(string Riesgo)
        {
            string color = string.Empty;
            if (Riesgo != string.Empty)
            {
                switch (Riesgo.ToUpper())
                {
                    case "FLECHAARRIBA":
                        color = "ROJO";
                        break;
                    case "FLECHADERECHA":
                        color = "AMARILLO";
                        break;
                    case "FLECHAABAJO":
                        color = "VERDE";
                        break;
                }
            }
            return color;
        }

        private string ComportamientoRiesgo(string programado, string real)
        {
            const string BAJO = "Bajo"; //VERDE
            const string MODERADO = "Moderado"; //AMARILLO
            const string ELEVADO = "Elevado"; //ROJO
            const string SUBE = "FlechaArriba"; //FLECHA ARRIBA
            const string BAJA = "FlechaAbajo"; //FLECHA ABAJO
            const string CONTINUA = "FlechaDerecha"; //FLECHA DERECHA


            string comportamiento = CONTINUA;

            //Si el riesgo programado es verde y el real es amarillo o rojo, el riesgo subió. (Flecha Arriba)
            if ((programado == BAJO) && (real == MODERADO || real == ELEVADO))
                comportamiento = SUBE;

            //Si el riesgo programado es amarillo y el real es  rojo, el riesgo subió. (Flecha Arriba)
            if (programado == MODERADO && real == ELEVADO)
                comportamiento = SUBE;

            // Si el riesgo programado y real es igual, el riesgo no cambió.(->)

            if (programado == real)
                comportamiento = CONTINUA;

            //Si el riesgo programado es Amarillo y el Real es Verde, el Riesgo bajó. (Flecha Abajo))
            if (programado == MODERADO && real == BAJO)
                comportamiento = BAJA;

            //Si el riesgo programado es Rojo y el Real es Amarillo o Verde, el Riesgo bajó. (Flecha Abajo).
            if ((programado == ELEVADO) && (real == MODERADO || real == BAJO))
                comportamiento = BAJA;


            return comportamiento;
        }

        private EntSemaforoVpn SemaforoDesviacionVpn(float VpnProg, float VpnReal)
        {
            double Desviacion = 0;
            string Semaforo = "Verde";
            //Desviación: ((vpnreal-vpnprogramado)/vpnprogramado)*100
            if (VpnProg != 0)
                Desviacion = ((VpnReal - VpnProg) / VpnProg) * 100;
            else
                Desviacion = 0;
            //Rojo: Desviación > 15
            if (Desviacion > 15)
                Semaforo = "Rojo";
            //Amarillo: Desviación > 0  , <=15
            if (Desviacion > 0 && Desviacion <= 15)
                Semaforo = "Amarillo";
            //Verde:  Desviación <=0 , >=-10
            if (Desviacion <= 0 && Desviacion >= -10)
                Semaforo = "Verde";
            //Azul: >-10
            if (Desviacion > -10)
                Semaforo = "Azul";


            EntSemaforoVpn semaforovpn = new EntSemaforoVpn();
            semaforovpn.Semaforo = Semaforo;
            semaforovpn.Valor = Math.Round(Desviacion);


            return semaforovpn;
        }

        private string getSemaforo(string p)
        {
            string colorS = "../../Public/img/Semaforos/Verde.png";
           
            if (p != string.Empty)
            {
                switch (p.ToUpper())
                {
                    case "ROJO":
                        colorS = "../../Public/img/Semaforos/Rojo.png";
                        break;
                    case "AMARILLO":
                        colorS = "../../Public/img/Semaforos/Amarillo.png";
                        break;
                    case "AZUL":
                        colorS = "../../Public/img/Semaforos/Azul.png";
                        break;
                }
            }
            return colorS;
        }
        #endregion

        #region Buttons
        protected void btnConcluido_Click(object sender, EventArgs e)
        {
            Response.Redirect("Tablero.aspx?filtro=Concluido");
        }

        protected void btnExe_Click(object sender, EventArgs e)
        {
            Response.Redirect("Tablero.aspx?filtro=En Proceso");
        }
        #endregion

    }
}