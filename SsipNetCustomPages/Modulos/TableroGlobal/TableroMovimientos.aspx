﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="TableroMovimientos.aspx.cs" Inherits="SsipTableros.Modulos.TableroGlobal.TableroMovimientos" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <meta http-equiv="X-UA-Compatible" content="IE=7,chrome=1" />
    <link href='../../Public/img/favicon_SS.ico' rel='shortcut icon' type='image/x-icon'>
    <title>SSIP - RMSO</title>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <!-- Bootstrap -->
    <link href="../../Public/css/bootstrap/css/bootstrap.min.css" rel="stylesheet" media="screen" />
    <link href="../../Public/css/blueprint/screen.css" rel="stylesheet" media="screen"/>
    <link href="../../Public/css/StyleSSIP.css" rel="stylesheet" media="screen" />
    <!-- Js -->
    <script src="../../Public/js/jquery/jquery.min.js"></script>
    <script src="../../Public/css/bootstrap/js/bootstrap.min.js"></script>
    <script src="../../Public/js/bowser-master/bowser.min.js"></script>
    <style type="text/css">
        html, body {

                margin: 0;
                padding: 0;
                color: #555;
                /* font-family: Arial, Helvetica, sans-serif; */
                font-family: Calibri;
                font-size: 11pt;
                outline: 0;
                border:0;
                height:100%;

            }
            label.error {

                color: red;

            }

        .meter-wrap{
            position: relative;
        }

        .meter-wrap, .meter-value, .meter-text {
            /* The width and height of your image */
            width: 150px; height: 20px;
        }

        .meter-wrap, .meter-value {
            background: #d9d9d9 url() top left no-repeat;
        }

        .meter-text {
            position: absolute;
            top:0; left:0;

            padding-top: 3px;

            color: #fff;
            text-align: center;
            width: 100%;
        }

    </style>
</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="True"
            EnableScriptLocalization="True" EnablePageMethods="true" />

        <!-- A partir de aquí, el contenido de la página -->
        <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Always">
            <ContentTemplate>
                <div class="gsg-container clearfix">
                    <!-- Barra de navegación de la sección -->
                    <div id="top-nav-fake" class="row-fluid">
                        <div class="navbar-fixed-top" id="top-nav">
                            <div class="span4 wp-23-ie lbl-seccion">
                                <img class="img-titulo" src="../../Public/img/iconoTitulos/tableros_icono.png" />
                                <span class="lbl-titulo-g">Tablero Movimientos</span>
                                <div class="muesca-right"></div>
                            </div>
                            <div class="span7">
                                <ul class="nav nav-pills center submenu-seccion" style="float:left;">
                                    <li><a href="Tablero.aspx" class="text-center">Todo</a></li>
                                    <li><a href="TableroIntervenciones.aspx" class="text-center">Intervenciones</a></li>
                                    <li class="active"><a href="#" class="text-center">Movimientos</a></li>
                                </ul>
                                <div class="span1">
                                    <div class="cnt-submenu-opc">
                                        <a class="img-btn btn-atras set-right mrg-l20 " href="~/Principal" onclick=" window.history.back()"></a>
                                    </div>
                                </div>
                                
                            </div>
                            <div class="span3">
                                <ul class="nav nav-pills center submenu-seccion">
                                    <li class="active" id="liEjecucion" runat="server">
                                        <asp:LinkButton ID="btnExe" runat="server" class="text-center">En Ejecución</asp:LinkButton>
                                    </li>
                                    <li id="liConcluida" runat="server">
                                        <asp:LinkButton ID="btnConcluido" runat="server" class="text-center">Concluidas</asp:LinkButton>
                                    </li>
                                </ul>                         
                            </div>                           
                        </div>                      
                        </div>
                    </div> <!-- Termina Barra de Navegación -->
                <div class="container-fluid" style="margin-top:100px; margin-left:-60px;">
                        <!--Movimientos -->
                        <div class="span-39 last" >
                            <div class="span-1" >
                                &nbsp;
                            </div>

                            <div class="span-46 last" >
	                            <div class="span-30 last TipoActividadTitulo" align="Center">
	                                MOVIMIENTO DE EQUIPOS
	                            </div>

	                            <div class="span-40 last titleTablas EncabezadoTitulosConsulta" >

		                            <div class="span-0">
		                                #
		                            </div>
                                    <div class="span-7">
		                                Equipo
		                            </div>
		                            <div class="span-5">
		                                Actividad
		                            </div>

		                            <div class="span-3">
		                                Fecha Inicio (POS)
		                            </div>

		                            <div class="span-3">
		                                Fecha Fin <br>(POS)
		                            </div>

		                            <div class="span-3">
		                                Fecha Fin (Programa)
		                            </div>

		                            <div class="span-5">
		                                Indicador Tiempo (Programa)
		                            </div>

		                            <div class="span-5">
		                                Indicador Avance (Programa)
		                            </div>                                  
                                    <div class="span-3">
		                               Riesgo <br>(Prog vs Real)
		                            </div>
	                            </div>

                                <%--Datos desde BD--%>
                                
                                        <div class="span-40 last ContenidoConsultaIndexindex BordeInferior" >
	                                        <div class="span-0">
	                                            <%--<asp:Label ID="idReg" runat="server" Text="0"></asp:Label>--%>
                                                1
	                                        </div>

	                                        <div class="span-7 PozoConsultaIndex">
	                                            <%--<a href="#"><span class="PozoConsultaIndex"><asp:Label ID="lbNombrePozo" runat="server" Text='<%# Eval("NombrePozo") %>'></asp:Label></span></a>--%>
                                                NOBLE CARL NORBERG A/E XXXVIII
	                                        </div>

	                                        <div class="span-5">
	                                           <%--<asp:Label ID="lbEquipo" runat="server" Text='<%# Eval("NombreEquipo") %>'></asp:Label>--%>
                                                MOV. TORRE E INTERCONEXION
	                                        </div>

	                                        <div class="span-3">
	                                            <%--<asp:Label ID="lbFechaInicio" runat="server" Text='<%# Eval("FechaInicio") %>'></asp:Label>--%>
                                                01/02/2014
	                                        </div>

	                                        <div class="span-3 FondoAzulConsulta">
	                                           <%--<asp:Label ID="lbFechaFin" runat="server" Text='<%# Eval("FechaTermino") %>'></asp:Label>--%>
                                                01/03/2014
	                                        </div>

	                                        <div class="span-3">
	                                            <%--<asp:Label ID="lbFechaFinP" runat="server" Text='<%# Eval("FechaTerminoProy") %>'></asp:Label>--%>
                                                07/07/2014
	                                        </div>

	                                        <div class="span-5">
	                                            <div class='span-1'>
                                                    <%--<asp:Image ID="imgSemaforo" runat="server" ImageUrl="~/Public/img/semaforos/Rojo.png"/>--%>
                                                    <img src="../../Public/img/semaforos/rojo.png" />
	                                            </div>
                                                +81.15d&nbsp;&nbsp;<%--(<asp:Label ID="lbPorcentaje" runat="server" Text='<%# Eval("Porcentaje") %>'></asp:Label>--%>80%)
	                                        </div>

	                                        <div class="span-5">
	                                            <table>
	                                            <tr>	
                                                    <td>
		                                                <div class="meter-wrap">
			   		                                         <div class="meter-value" style="background-color: #92d050; width: 97.37%;">
				                                                <div class="meter-text">

				                                                </div>
					                                        </div>
				                                        </div>
			                                        </td>
			                                        <td>
			                                          <b><%--<%# Eval("Avance")%--%> 97.37%</b>
			                                        </td>
		                                        </tr>
		                                        </table>
	                                        </div>                                           

                                            <div class="span-3">
	                                            <div class='span-3'>
                                                    <%--<asp:Image ID="imgRProg" runat="server" ImageUrl="../../Public/img/semaforos/Verde.png" /> / <asp:Image ID="imgRReal" runat="server" ImageUrl="../../Public/img/semaforos/Verde.png" />--%>
                                                    <div class="cRiesgo" style="background-color: red; float:left;">Elevado</div>
                                                    <img src="../../Public/img/flecha_arriba.png" />
	                                            </div>
	                                        </div>
                                    </div>                              
                            </div>
                        </div> <!-- Termina Movimientos -->
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
    </form>
</body>
</html>
