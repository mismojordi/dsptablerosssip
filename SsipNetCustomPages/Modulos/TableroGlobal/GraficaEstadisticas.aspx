﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="GraficaEstadisticas.aspx.cs" Inherits="SsipTableros.Modulos.TableroGlobal.GraficaEstadisticas" %>
<script src="../../Public/js/jquery/jquery-1.8.3.js"></script>
<script src="../../Public/js/Highcharts3/highcharts.js"></script>
<script>
    //obtener las variables c#    
   
    <%  string Pozo=ViewState["Pozo"].ToString(); %>
    <%  string CodigoColumnas= ViewState["CodigoColumnas"].ToString(); %>
    <%  string CodigoPastel= ViewState["CodigoPastel"].ToString(); %>
    <%  string CodigoAreas = ViewState["CodigoAreas"].ToString(); %>
    //para tabla de dato totales
    <%  float[] Totales = (float[]) ViewState["Totales"]; %>
    //para tabla de dato porcientos
    <%  float[] Porcientos = (float[]) ViewState["Porcientos"]; %>
</script>

 <script type="text/javascript">
     //Grafica de Pasteles para la distribucion de tiempos Total
     // Build the chart
     $(function () {

         $('#containerPasteles').highcharts({
             chart: {
                 renderTo: 'containerPasteles',
                 backgroundColor: 'none'
             },
             title: {
                 text: 'Distribuci\u00f3n de Tiempos en porcentaje'
             },
             subtitle: {
                 text: '<%=Pozo%>'
             },
             tooltip: {
                 pointFormat: '{series.name}: <b>{point.percentage:.2f}%</b>'
             },
             plotOptions: {
                 pie: {
                     allowPointSelect: true,
                     cursor: 'pointer',
                     dataLabels: {
                         enabled: true,
                         color: '#000000',
                         connectorColor: '#000000',
                         format: '<b>{point.name}</b>: {point.percentage:.2f} %',
                     },
                     showInLegend: true
                 }
             },
             legend: {
                 layout: 'vertical',
                 align: 'left',
                 verticalAlign: 'middle',
                 y: 40
             },
             credits: {
                 enabled: false
             },
             series: [{
                 type: 'pie',
                 name: 'Operaciones',
                 data: [
                        //imprimimos los datos
                        <%=CodigoPastel %>
                 ]
             }]
         });




//fin de la grafica
     });

</script>

<script type="text/javascript">

   
    //Grafica de Pasteles para la distribucion de tiempos Total
    // Build the chart
    $(function () {

        $('#containerMes').highcharts({
            
            chart: {
                renderTo: 'containerMes'
            },
            credits: {
                enabled: false
            },
            title: {
                text: 'Distribuci\u00f3n de Tiempos (Días)'
            },
            legend: { enabled: false },
            subtitle: {
                text: '<%=Pozo %>'
            },
            xAxis: {
                categories: [],
                labels: { enabled: false },
                title: {
                    text: 'Operaciones'
                }
            },
            yAxis: {
                min: 0,
                title: {
                    text: 'D\u00edas',
                },
                labels: {
                    overflow: 'justify'
                }
            },
            tooltip: {
                enabled: false
            },
            plotOptions: {
                column: {
                    dataLabels: {
                        enabled: true,
                        color: '#4974a7',
                        style: {
                            fontWeight: 'normal'
                        },
                        format: '{point.percentage:.2f} %',
                    }
                }
            },
            series: [

            //Empezamos a imprimir las series
            <%= CodigoColumnas %>
      
            ]
        });



        //fin de la grafica
    });

</script>

<script>
     
    /////////Dibujamos la grafica de area Total
    $(function () {

        $('#containerLineal').highcharts({
            chart: {
                renderTo: 'containerLineal',
                zoomType: 'x',
                spacingRight: 20
            },
            title: {
                text: 'Distribuci\u00f3n de Tiempos en Horas'
            },
            subtitle: {
                text: '<%=Pozo %>'
            },
            credits: {
                enabled: false
            },
            xAxis: {
                type: 'datetime',
                minRange: 10 * 24 * 3600000,
                dateTimeLabelFormats: { // don't display the dummy year
                    month: '%e. %b',
                    year: '%b'
                }
            },
            yAxis: {
                title: {
                    text: 'Horas'
                }
            },
            plotOptions: {
                area: {
                    marker: {
                        enabled: false
                    }
                }
            },
            series: [

                //imprimimos las series
                 <%= CodigoAreas %>


                     ]
        });

    });


</script>

<script>

    // CICLO DE graficas por  cada operacion Normal, Problemas, etc.
 
  
    <% string[] ArrayColumnas = (string[])ViewState["ArrayColumnas"]; %>
    <% string[] ArrayPastel = (string[])ViewState["ArrayPastel"]; %>
    <% string[] ArrayArea = (string[])ViewState["ArrayAreas"]; %>
    <% string[] containercolumnas = (string[])ViewState["containercolumnas"]; %>
    <% string[] containerpastel = (string[])ViewState["containerpastel"]; %>
    <% string[] containerarea =  (string[]) ViewState["containerarea"] ; %>
    <% string[] operaciones = (string[])ViewState["operaciones"]; %>
    <%string[] actividades = (string[])ViewState["actividades"]; %>
    <%var Intervencion = ViewState["Intervencion"]; %>
    //Tabla de datos
    <% System.Collections.Generic.List<TablaOperaciones> Tabla = (System.Collections.Generic.List<TablaOperaciones>)ViewState["ListaTablas"];  %>
    
    <%int color = -1; %>
    <%  for (int i = 0; i <= 3; i++ )
        { 
            if (ArrayColumnas[i] != "" )
            {
                color++;
                        
     %>
    //GRAFICA COLUMNAS
    $(function () {
        chart = new Highcharts.Chart({
            chart: {
                renderTo: '<%=containercolumnas[i]%>',
                zoomType: 'xy'
            },
            credits: {
                enabled: false
            },
            title: {
                text: '<%=Intervencion %>' + ':' + '<%= actividades[i] %>' + '(Días)'
            },
            subtitle: {
                text: '<%= Pozo %>'
            },
            legend: {
                layout: 'vertical',
                align: 'right',
                verticalAlign: 'middle',
                y: 40
            },
            xAxis: {
                categories: [],
                labels: { enabled: false },
                title: {
                    text: 'Operaciones'
                }
            },
            yAxis: {
                min: 0,
                title: {
                    text: 'D\u00edas'
                },
                labels: {
                    overflow: 'justify'
                }
            },


            tooltip: {
                enabled: false
            },
            plotOptions: {
                column: {

                    dataLabels: {
                        enabled: true,
                        color: '#4974a7',
                        style: {
                            fontWeight: 'normal'
                        },
                        format: '{point.y:.1f} <br>días',
                    }
                }


            },
            labels: {
                items: [{
                    html: '',
                    style: {
                        left: '40px',
                        top: '8px',
                        color: 'black'
                    }
                }]
            },
            series: [
               <%= ArrayColumnas[i] %>               
            ]

        })



    });

    //GRAFICA DE PASTEL

    $(function () {
        chart = new Highcharts.Chart({
            //Dibujamos el chart de Pasteles			  
            chart: {
                renderTo: '<%=containerpastel[i]%>',
                backgroundColor: 'none'
            },
            title: {
                text: '<%= actividades[i] %>' + ' en porcentaje'
            },
            subtitle: {
                text: '<%=Pozo%>'
            },
            tooltip: {
                format: '{point.percentage:.2f} %',

            },
            plotOptions: {
                pie: {
                    allowPointSelect: true,
                    cursor: 'pointer',
                    dataLabels: {
                        enabled: false
                    },
                    showInLegend: true
                }
            },
            legend: {
                layout: 'vertical',
                align: 'left',
                verticalAlign: 'top',
                y: 40
            },
            credits: {
                enabled: false
            },
            series: [{
                type: 'pie',
                name: 'Operaciones',
                data: [
                         <%= ArrayPastel[i] %>        
                ]
            }]

        })
    });
   
    //GRAFICA DE AREA

    $(function () {
        chart3 = new Highcharts.Chart({
            chart: {
                renderTo: '<%=containerarea[i]%>',
                zoomType: 'x',
                spacingRight: 20
            },
            title: {
                text: 'Distribución de horas - ' + '<%= actividades[i] %>'
            },
            subtitle: {
                text: '<%=Pozo%>'
            },
            credits: {
                enabled: false
            },
            xAxis: {
                type: 'datetime',
                minRange: 10 * 24 * 3600000,
                dateTimeLabelFormats: { // don't display the dummy year
                    month: '%e. %b',
                    year: '%b'
                }
            },
            yAxis: {
                title: {
                    text: 'Horas'
                }
            },
            legend: {
                enabled: false
            },
            plotOptions: {
                area: {
                    marker: {
                        enabled: false
                    }
                }
            },
            series: [{
                type: "area",
                name: '<%= actividades[i] %>',
                color: Highcharts.getOptions().colors[<%=color%>],
                data: [
                     <%= ArrayArea[i] %>  
                 
                ]
            }]
        })
    });


    <%
    }//end if
}%> //end for
</script>


			<div class="graficasOperaciones">
			
			<table cellspacing="0" cellpadding="0" width="1250">
			<tr>
				<td><div id="containerMes" style="width: 550px;"></div></td>
				<td><div id="containerPasteles" style="width:700px;"></div></td>
			
			</tr>
			</table>
			<div id="containerLineal" style="width:1200px;"></div>
			</div>

     <div class="graficasOperaciones" style="margin-top: 70px;">
				<div class="TituloDatos">Distribuci&oacute;n de Tiempo de Operaciones Normales (<% =Totales[0] %> d&iacute;as)</div>
				<div id="containerOPN" style="width: 1200px;"></div>

                <table cellspacing="0" cellpadding="0" width="1250">
				<tr>
					<td><div id="containerPasteles0" style="width:700px;"></div></td>

                </tr>
            </table>
            <div id="containerArea0" style="width: 1200px;"></div>
</div>

<div class="graficasOperaciones" style="margin-top: 80px;">
				<div class="TituloDatos">Distribuci&oacute;n de Tiempo de Operaciones con Problemas (<% =Totales[1] %> d&iacute;as)</div>
				<div id="containerOPC" style="width: 1200px;"></div>
				<table cellspacing="0" cellpadding="0" width="1250">
				<tr>
					<td><div id="containerPasteles1" style="width:700px;"></div></td>
                </tr>
                </table>
    <div id="containerArea1" style="width: 1200px;"></div>
    </div>


<div class="graficasOperaciones" style="margin-top: 80px;">
				<div class="TituloDatos">Distribuci&oacute;n de Tiempo de Operaciones Adicionales (<% =Totales[2] %> d&iacute;as)</div>
				<div id="containerOPA" style="width: 1200px; "></div>
				<table cellspacing="0" cellpadding="0" width="1250">
				    <tr>
					    <td><div id="containerPasteles2" style="width:700px; "></div></td>
                    </tr>
                </table>
    <div id="containerArea2" style="width: 1200px;"></div>

</div>

<div class="graficasOperaciones" style="margin-top: 80px;">
				<div class="TituloDatos">Distribuci&oacute;n de Tiempo de Esperas (<% =Totales[3] %> d&iacute;as)</div>
				<div id="containerESP" style="height: <?php echo $this->alto; ?>;width: 1200px; "></div>
				<table cellspacing="0" cellpadding="0" width="1250">
				<tr>
					<td><div id="containerPasteles3" style="width:700px;"></div></td>
                </tr>
                </table>

    <div id="containerArea3" style="width: 1200px;"></div>
    </div>