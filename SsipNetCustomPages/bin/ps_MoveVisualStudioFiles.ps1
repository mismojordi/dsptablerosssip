﻿param ($configFile)

$source = "C:\Users\Administrator\Documents\DSP\SolTablerosSSIP\SsipNetCustomPages"
$dest = "C:\Program Files\Common Files\Microsoft Shared\Web Server Extensions\14\TEMPLATE\LAYOUTS\AssetObserver\Pages\CustomPages\SsipTableros"
$files = @("*.aspx","*.aspx.cs","*.js")

$cmdArgs = @("$source","$dest",$files)

#Move the aspx and cs files to CustomPages folder
robocopy @cmdArgs

$options = @("-r")
$cmdArgs = @($options,"$dest\*.aspx")

#As the aspx files might have the attribute 'Read-only' set to true, we should set such attribute to false so that the 'Codebehind=' text be changed.
attrib $cmdArgs

#Find the text 'Codebehind=' and replace it for 'CodeFile='
get-childitem $dest *.aspx -recurse |
    Foreach-Object {
        $c = ($_ | Get-Content) 
        $c = $c -replace 'CodeBehind=','CodeFile='
        [IO.File]::WriteAllText($_.FullName, ($c -join "`r`n"))
    }

$sourceDLL = "C:\Users\Administrator\Documents\DSP\SolTablerosSSIP\SsipNetCustomPages\bin"
$destDLL = "C:\inetpub\wwwroot\wss\VirtualDirectories\9122\bin"
$filesDLL = @("SsipTableros*.dll")
$cmdArgsDLL = @("$sourceDLL","$destDLL",$filesDLL)

#Move the aspx and cs files to CustomPages folder
robocopy @cmdArgsDLL
