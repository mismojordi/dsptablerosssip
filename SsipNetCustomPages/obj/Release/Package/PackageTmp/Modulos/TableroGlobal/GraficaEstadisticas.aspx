﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="GraficaEstadisticas.aspx.cs" Inherits="SsipTableros.Modulos.TableroGlobal.GraficaEstadisticas" %>
<script src="../../Public/js/jquery/jquery-1.8.3.js"></script>
<script src="../../Public/js/Highcharts3/highcharts.js"></script>
<script>
    //obtener las variables c#    
    <%  string Pozo=ViewState["Pozo"].ToString(); %>
    <%  string CodigoColumnas= ViewState["CodigoColumnas"].ToString(); %>
    <%  string CodigoPastel= ViewState["CodigoPastel"].ToString(); %>
</script>

 <script type="text/javascript">
     //Grafica de Pasteles para la distribucion de tiempos
     // Build the chart
     $(function () {

         $('#containerPasteles').highcharts({
             chart: {
                 renderTo: 'containerPasteles',
                 backgroundColor: 'none'
             },
             title: {
                 text: 'Distribuci\u00f3n de Tiempos en porcentaje'
             },
             subtitle: {
                 text: 'Pozo IXTAL-83 (del  11/12/2013  al  22/05/2014 )'
             },
             tooltip: {
                 pointFormat: '{series.name}: <b>{point.percentage:.2f}%</b>'
             },
             plotOptions: {
                 pie: {
                     allowPointSelect: true,
                     cursor: 'pointer',
                     dataLabels: {
                         enabled: true,
                         color: '#000000',
                         connectorColor: '#000000',
                         format: '<b>{point.name}</b>: {point.percentage:.2f} %',
                     },
                     showInLegend: true
                 }
             },
             legend: {
                 layout: 'vertical',
                 align: 'left',
                 verticalAlign: 'middle',
                 y: 40
             },
             credits: {
                 enabled: false
             },
             series: [{
                 type: 'pie',
                 name: 'Operaciones',
                 data: [
                        //Empieza la carga dinamica
                        <%=CodigoPastel %>
                      //  ['Operaciones Normales', 61.337423312883], ['Operaciones con Problemas', 3.5889570552147], ['Esperas', 35.073619631902],
                     //Termina la carga dinamica
                 ]
             }]
         });




//fin de la grafica
     });

</script>

<script type="text/javascript">

   
    //Grafica de Pasteles para la distribucion de tiempos
    // Build the chart
    $(function () {

        $('#containerMes').highcharts({
            
            chart: {
                renderTo: 'containerMes'
            },
            credits: {
                enabled: false
            },
            title: {
                text: 'Distribuci\u00f3n de Tiempos (Días)'
            },
            legend: { enabled: false },
            subtitle: {
                text: '<%=Pozo %>'
            },
            xAxis: {
                categories: [],
                labels: { enabled: false },
                title: {
                    text: 'Operaciones'
                }
            },
            yAxis: {
                min: 0,
                title: {
                    text: 'D\u00edas',
                },
                labels: {
                    overflow: 'justify'
                }
            },
            tooltip: {
                enabled: false
            },
            plotOptions: {
                column: {
                    dataLabels: {
                        enabled: true,
                        color: '#4974a7',
                        style: {
                            fontWeight: 'normal'
                        },
                        format: '{point.y:.1f} <br>días',
                        
                    }
                }
            },
            series: [

            //Empezamos a imprimir las series
            <%= CodigoColumnas %>
          //  { type: 'column', name: 'Operaciones Normales', shadow: false, data: [{ y: 99.98 }] } , { type: 'column', name: 'Operaciones con Problemas', shadow: false, data: [{ y: 5.85 }] }, { type: 'column', name: 'Esperas', shadow: false, data: [{ y: 57.17 }] }
      
            ]
        });



        //fin de la grafica
    });

</script>


			<div class="graficasOperaciones">
			
			<table cellspacing="0" cellpadding="0" width="1250">
			<tr>
				<td><div id="containerMes" style="width: 550px;"></div></td>
				<td><div id="containerPasteles" style="width:700px;"></div></td>
			
			</tr>
			</table>
			<div id="containerLineal" style="width:1200px;"></div>
			</div>