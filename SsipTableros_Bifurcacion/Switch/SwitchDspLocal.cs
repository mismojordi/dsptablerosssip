﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using Dsp.Sdk;
using Dsp.SP.ClientManager;
using System.Data.Odbc;

namespace Bifurcacion.Switch
{


    public class SwitchDspLocal
    {
        #region intervenciones
        //obtiene las intervenciones de las dos bd y los mezcal en un datatable
        public DataTable IntervencionesMerge(string Estatus)
        {
          //intervebcinoes de ssiabk
           DataTable dtAbk=  RecuperarIntervenciones(Estatus, "Abkatun");
            //intervenciones de ssiplitoral
           DataTable dtLit = RecuperarIntervenciones(Estatus, "Litoral");
        //mesclamos y listo
           dtAbk.Merge(dtLit);
        //nos quedo un datatable con las dos bds.
           return dtAbk;
       }

        //obtiene las intervenciones de las dos bd y los mezcal en un datatable
        public DataTable IntervencionesMerge(string Estatus, bool DSP)
        {
            //intervebcinoes de ssiabk
            DataTable dtAbk = RecuperarIntervenciones(Estatus, "Abkatun", true);
            //intervenciones de ssiplitoral
            DataTable dtLit = RecuperarIntervenciones(Estatus, "Litoral", true);
            //mesclamos y listo
            dtAbk.Merge(dtLit);
            //nos quedo un datatable con las dos bds.
            return dtAbk;
        }



        //funcion local para regresar las intervenciones de abk o litoral
         public DataTable RecuperarIntervenciones(string Estatus, string Activo) 
         {
            //en base al activo definir la bd a la que hace la consulta.
             string bd = DefinirBD(Activo);
            //conexion por ODBC
            OdbcConnection cn = conectarODBC();          
            //dataset para llenado
            DataSet dt = new DataSet();
            ///datatable para result
            DataTable dtResult = new DataTable();     
            //query para el store
            string store = "{call " +  bd +".sp_TabIntervencionesEstatus(?)}"; //Editable
            //comando para ejecutar
            OdbcCommand cmd = new OdbcCommand(store, cn);  
            //parametros del store
            OdbcParameter prm = cmd.Parameters.Add("pEstatus", OdbcType.VarChar, 20);      //Editable       
             //valor del parametro
            prm.Value = Estatus;        
            //pasar el comando al adapter
            OdbcDataAdapter da = new OdbcDataAdapter(cmd);
            //llenar el dataset
            da.Fill(dt);
            //reuperar en datatable
            dtResult =  dt.Tables[0];   
            //retornar
            return dtResult;
             
        }


         //funcion DSP para regresar las intervenciones de abk y litoral
         public DataTable RecuperarIntervenciones(string Estatus, string Activo, bool DSP)
         {
             //en base al activo definir la bd a la que hace la consulta.
             string bd = DefinirBD(Activo);
             //datatable para el return
             DataTable dtResult = new DataTable();
             //datasource
             string sDataSourceID = "System.Sources.Db.Odbc.Mysql_ssipabk";
             //comando
             string sQueryID = "System.Sources.Db.Odbc.Mysql_ssipabk.Queries.sp_TabIntervencionesEstatus.Query";
             //parametros
             IList<string> ilParameters = new List<string>();
             IRelationalDataService _dbQueryService = RelationalDataClientManager.RelationalDataService;
             //agregar parametro
             ilParameters.Add(bd);
             ilParameters.Add(Estatus);
             //ejecutar comando
             dtResult = _dbQueryService.QueryForData(sDataSourceID, sQueryID, ilParameters);
             //retornar
             return dtResult;

         }




         //funcion LOCAL para regresar el resumen de una intervencion de abk y litoral
         public DataTable ResumenIntervencion(int IdIntervencion, string Activo)
         {
             //en base al activo definir la bd a la que hace la consulta.
             string bd = DefinirBD(Activo);
             //conexion por ODBC
             OdbcConnection cn = conectarODBC();
             //dataset para llenado
             DataSet dt = new DataSet();
             ///datatable para result
             DataTable dtResult = new DataTable();
             //query para el store
             string store = "{call " + bd + ".sp_TabResumenIntervencion(?)}"; //Editable
             //comando para ejecutar
             OdbcCommand cmd = new OdbcCommand(store, cn);
             //parametros del store
             OdbcParameter prm = cmd.Parameters.Add("pIdIntervencion", OdbcType.Int);      //Editable       
             //valor del parametro
             prm.Value = IdIntervencion;
             //pasar el comando al adapter
             OdbcDataAdapter da = new OdbcDataAdapter(cmd);
             //llenar el dataset
             da.Fill(dt);
             //reuperar en datatable
             dtResult = dt.Tables[0];
             //retornar
             return dtResult;

         }



         //funcion DSP para regresar el resumen de una intervencion de abk y litoral
         public DataTable ResumenIntervencion(int IdIntervencion, string Activo, bool DSP)
         {
             //en base al activo definir la bd a la que hace la consulta.
             string bd = DefinirBD(Activo);
             //datatable para el return
             DataTable dtResult = new DataTable();
             //datasource
             string sDataSourceID = "System.Sources.Db.Odbc.Mysql_ssipabk";
             //comando
             string sQueryID = "System.Sources.Db.Odbc.Mysql_ssipabk.Queries.sp_TabResumenIntervencion.Query";
             //parametros
             IList<string> ilParameters = new List<string>();
             IRelationalDataService _dbQueryService = RelationalDataClientManager.RelationalDataService;
             //agregar parametro
             ilParameters.Add(bd);
             ilParameters.Add(IdIntervencion.ToString());
             //ejecutar comando
             dtResult = _dbQueryService.QueryForData(sDataSourceID, sQueryID, ilParameters);
             //retornar
             return dtResult;

         }

    #endregion


    #region imagenes

         //funcion LOCAL para regresar la lista de estados mecanicos o graficas de profundidad de un intervencion
        //Tipo= graficaprofvstiempo o estadomecanico
         public DataTable ConsultarImagenes(int IdIntervencion, string Activo, string Tipo)
         {
             //en base al activo definir la bd a la que hace la consulta.
             string bd = DefinirBD(Activo);
             //conexion por ODBC
             OdbcConnection cn = conectarODBC();
             //dataset para llenado
             DataSet dt = new DataSet();
             ///datatable para result
             DataTable dtResult = new DataTable();
             //query para el store
             string store = "{call " + bd + ".sp_TabConsultaImagenesIntervencion(?,?)}"; //Editable
             //comando para ejecutar
             OdbcCommand cmd = new OdbcCommand(store, cn);
             //parametros del store
             OdbcParameter prm = cmd.Parameters.Add("pTipo", OdbcType.VarChar,20);      //Editable       
             //valor del parametro
             prm.Value = Tipo;
             //parametros del store
             OdbcParameter prm1 = cmd.Parameters.Add("pidIntervencion", OdbcType.Int);      //Editable       
             //valor del parametro
             prm1.Value = IdIntervencion;
             //pasar el comando al adapter
             OdbcDataAdapter da = new OdbcDataAdapter(cmd);
             //llenar el dataset
             da.Fill(dt);
             //reuperar en datatable
             dtResult = dt.Tables[0];
             //retornar
             return dtResult;

         }



         //funcion DSP para regresar la lista de estados mecanicos o graficas de profundidad de un intervencion
         //Tipo= graficaprofvstiempo o estadocanico
         public DataTable ConsultarImagenes(int IdIntervencion, string Activo, string Tipo, bool DSP)
         {
             //en base al activo definir la bd a la que hace la consulta.
             string bd = DefinirBD(Activo);
             //datatable para el return
             DataTable dtResult = new DataTable();
             //datasource
             string sDataSourceID = "System.Sources.Db.Odbc.Mysql_ssipabk";
             //comando
             string sQueryID = "System.Sources.Db.Odbc.Mysql_ssipabk.Queries.sp_TabConsultaImagenesIntervencion.Query";
             //parametros
             IList<string> ilParameters = new List<string>();
             IRelationalDataService _dbQueryService = RelationalDataClientManager.RelationalDataService;
             //agregar parametro
             ilParameters.Add(bd);
             ilParameters.Add(Tipo);
             ilParameters.Add(IdIntervencion.ToString());
             //ejecutar comando
             dtResult = _dbQueryService.QueryForData(sDataSourceID, sQueryID, ilParameters);
             //retornar
             return dtResult;

         }
#endregion
    #region estadisticas


         //funcion LOCAL para regresar las estadisticas TOTALES en DIAS
         //GrupoActividad = OPN, OPC, OPA, ESP, 
         public DataTable EstadisticasDiasTotales(int IdIntervencion, string Activo, DateTime FechaInicio, DateTime FechaFin)
         {
             //en base al activo definir la bd a la que hace la consulta.
             string bd = DefinirBD(Activo);
             //conexion por ODBC
             OdbcConnection cn = conectarODBC();


             //Definir las fechas
             if (FechaInicio == new DateTime(1900, 1, 1))
             {
                 DataTable dtFechas = new DataTable();
                 dtFechas = FechasMinMaxEstadisticas(IdIntervencion, Activo);
                 FechaInicio = DateTime.Parse(dtFechas.Rows[0]["FechaMin"].ToString());
                 FechaFin = DateTime.Parse(dtFechas.Rows[0]["FechaMax"].ToString());
             }
             //dataset para llenado
             DataSet dt = new DataSet();
             ///datatable para result
             DataTable dtResult = new DataTable();
             //query para el store
             string store = "{call " + bd + ".sp_SelectDuracionGrupoActividadTotal(?,?,?)}"; //Editable
             //comando para ejecutar
             OdbcCommand cmd = new OdbcCommand(store, cn);
             //parametros del store
             OdbcParameter prm = cmd.Parameters.Add("pidIntervencion", OdbcType.Int);      //Editable       
             //valor del parametro
             prm.Value = IdIntervencion;
             //parametros del store
             OdbcParameter prm2 = cmd.Parameters.Add("pFechaInicio", OdbcType.Date);      //Editable       
             //valor del parametro
             prm2.Value = FechaInicio;
             //parametros del store
             OdbcParameter prm3 = cmd.Parameters.Add("pFechaFin", OdbcType.Date);      //Editable       
             //valor del parametro
             prm3.Value = FechaFin;
             //pasar el comando al adapter
             OdbcDataAdapter da = new OdbcDataAdapter(cmd);
             //llenar el dataset
             da.Fill(dt);
             //reuperar en datatable
             dtResult = dt.Tables[0];
             //retornar
             return dtResult;

         }



         //funcion LOCAL para regresar las estadisticas TOTALES en DIAS
         //GrupoActividad = OPN, OPC, OPA, ESP, 
         public DataTable EstadisticasDiasTotales(int IdIntervencion, string Activo, DateTime FechaInicio, DateTime FechaFin, bool DSP)
         {
             //en base al activo definir la bd a la que hace la consulta.
             string bd = DefinirBD(Activo);
             //datatable para el return
             DataTable dtResult = new DataTable();
             //datasource
             string sDataSourceID = "System.Sources.Db.Odbc.Mysql_ssipabk";
             //comando
             string sQueryID = "System.Sources.Db.Odbc.Mysql_ssipabk.Queries.sp_SelectDuracionGrupoActividadTotal.Query";
             //parametros
             IList<string> ilParameters = new List<string>();
             IRelationalDataService _dbQueryService = RelationalDataClientManager.RelationalDataService;
             //agregar parametro
             ilParameters.Add(bd);
             ilParameters.Add(IdIntervencion.ToString());            
             ilParameters.Add(FechaInicio.ToString());
             ilParameters.Add(FechaFin.ToString());

             //ejecutar comando
             dtResult = _dbQueryService.QueryForData(sDataSourceID, sQueryID, ilParameters);
             //retornar
             return dtResult;

         }






         //funcion LOCAL para regresar las estadisticas en DIAS
         //GrupoActividad = OPN, OPC, OPA, ESP, 
         public DataTable DuracionGrupoActividades(int IdIntervencion, string Activo, string GrupoActividad, DateTime FechaInicio, DateTime FechaFin)
         {
             //en base al activo definir la bd a la que hace la consulta.
             string bd = DefinirBD(Activo);
             //conexion por ODBC
             OdbcConnection cn = conectarODBC();


             //Definir las fechas
             if (  FechaInicio ==  new DateTime(1900,1,1) )
             {
                 DataTable dtFechas = new DataTable();
                 dtFechas = FechasMinMaxEstadisticas(IdIntervencion, Activo);
                 FechaInicio = DateTime.Parse(dtFechas.Rows[0]["FechaMin"].ToString());
                 FechaFin = DateTime.Parse(dtFechas.Rows[0]["FechaMax"].ToString());
            }
             //dataset para llenado
             DataSet dt = new DataSet();
             ///datatable para result
             DataTable dtResult = new DataTable();
             //query para el store
             string store = "{call " + bd + ".sp_SelectDuracionGrupoActividadIntervencion(?,?,?,?)}"; //Editable
             //comando para ejecutar
             OdbcCommand cmd = new OdbcCommand(store, cn);
             //parametros del store
             OdbcParameter prm = cmd.Parameters.Add("pidIntervencion", OdbcType.Int);      //Editable       
             //valor del parametro
             prm.Value = IdIntervencion;
             //parametros del store
             OdbcParameter prm1 = cmd.Parameters.Add("pGrupoActividad", OdbcType.VarChar,4);      //Editable       
             //valor del parametro
             prm1.Value = GrupoActividad;
             //parametros del store
             OdbcParameter prm2 = cmd.Parameters.Add("pFechaInicio", OdbcType.Date);      //Editable       
             //valor del parametro
             prm2.Value = FechaInicio;
             //parametros del store
             OdbcParameter prm3 = cmd.Parameters.Add("pFechaFin", OdbcType.Date);      //Editable       
             //valor del parametro
             prm3.Value = FechaFin;
             //pasar el comando al adapter
             OdbcDataAdapter da = new OdbcDataAdapter(cmd);
             //llenar el dataset
             da.Fill(dt);
             //reuperar en datatable
             dtResult = dt.Tables[0];
             //retornar
             return dtResult;

         }



         //funcion DSP para regresar las estadisticas en DIAS
         //GrupoActividad = OPN, OPC, OPA, ESP, 
         public DataTable DuracionGrupoActividades(int IdIntervencion, string Activo, string GrupoActividad, DateTime FechaInicio, DateTime FechaFin, bool DSP)
         {
             //en base al activo definir la bd a la que hace la consulta.
             string bd = DefinirBD(Activo);
             //datatable para el return
             DataTable dtResult = new DataTable();
             //datasource
             string sDataSourceID = "System.Sources.Db.Odbc.Mysql_ssipabk";
             //comando
             string sQueryID = "System.Sources.Db.Odbc.Mysql_ssipabk.Queries.sp_SelectDuracionGrupoActividadIntervencion.Query";
             //parametros
             IList<string> ilParameters = new List<string>();
             IRelationalDataService _dbQueryService = RelationalDataClientManager.RelationalDataService;
             //agregar parametro
             ilParameters.Add(bd);
             ilParameters.Add(IdIntervencion.ToString());
             ilParameters.Add(GrupoActividad);
             ilParameters.Add(FechaInicio.ToString());
             ilParameters.Add(FechaFin.ToString());
        
             //ejecutar comando
             dtResult = _dbQueryService.QueryForData(sDataSourceID, sQueryID, ilParameters);
             //retornar
             return dtResult;

         }



         //funcion LOCAL para regresar las estadisticas en DIAS TODAS
         //GrupoActividad = OPN, OPC, OPA, ESP, 
         public DataTable HorasDiasOperacionesTodas(int IdIntervencion, string Activo, DateTime FechaInicio, DateTime FechaFin)
         {
             //en base al activo definir la bd a la que hace la consulta.
             string bd = DefinirBD(Activo);
             //conexion por ODBC
             OdbcConnection cn = conectarODBC();
             //dataset para llenado
             DataSet dt = new DataSet();
             ///datatable para result
             DataTable dtResult = new DataTable();
             //query para el store
             string store = "{call " + bd + ".sp_SelectHorasDiasOperacionesTodas(?,?,?)}"; //Editable
             //comando para ejecutar
             OdbcCommand cmd = new OdbcCommand(store, cn);
             //parametros del store
             OdbcParameter prm = cmd.Parameters.Add("pidIntervencion", OdbcType.Int);      //Editable       
             //valor del parametro
             prm.Value = IdIntervencion;
             //parametros del store
             OdbcParameter prm2 = cmd.Parameters.Add("pFechaInicio", OdbcType.Date);      //Editable       
             //valor del parametro
             prm2.Value = FechaInicio;
             //parametros del store
             OdbcParameter prm3 = cmd.Parameters.Add("pFechaFin", OdbcType.Date);      //Editable       
             //valor del parametro
             prm3.Value = FechaFin;
             //pasar el comando al adapter
             OdbcDataAdapter da = new OdbcDataAdapter(cmd);
             //llenar el dataset
             da.Fill(dt);
             //reuperar en datatable
             dtResult = dt.Tables[0];
             //retornar
             return dtResult;

         }



         //funcion DSP para regresar las estadisticas en DIAS
         //GrupoActividad = OPN, OPC, OPA, ESP, 
         public DataTable HorasDiasOperacionesTodas(int IdIntervencion, string Activo, DateTime FechaInicio, DateTime FechaFin, bool DSP)
         {
             //en base al activo definir la bd a la que hace la consulta.
             string bd = DefinirBD(Activo);
             //datatable para el return
             DataTable dtResult = new DataTable();
             //datasource
             string sDataSourceID = "System.Sources.Db.Odbc.Mysql_ssipabk";
             //comando
             string sQueryID = "System.Sources.Db.Odbc.Mysql_ssipabk.Queries.sp_SelectHorasDiasOperacionesTodas.Query";
             //parametros
             IList<string> ilParameters = new List<string>();
             IRelationalDataService _dbQueryService = RelationalDataClientManager.RelationalDataService;
             //agregar parametro
             ilParameters.Add(bd);
             ilParameters.Add(IdIntervencion.ToString());            
             ilParameters.Add(FechaInicio.ToString());
             ilParameters.Add(FechaFin.ToString());

             //ejecutar comando
             dtResult = _dbQueryService.QueryForData(sDataSourceID, sQueryID, ilParameters);
             //retornar
             return dtResult;

         }






         //funcion LOCAL para regresar las estadisticas en HORAS
         //GrupoActividad = OPN, OPC, OPA, ESP, 
         public DataTable HorasDiasGrupoActividades(int IdIntervencion, string Activo, string GrupoActividad, DateTime FechaInicio, DateTime FechaFin)
         {
             //en base al activo definir la bd a la que hace la consulta.
             string bd = DefinirBD(Activo);
             //conexion por ODBC
             OdbcConnection cn = conectarODBC();
             //dataset para llenado
             DataSet dt = new DataSet();
             ///datatable para result
             DataTable dtResult = new DataTable();
             //query para el store
             string store = "{call " + bd + ".sp_SelectHorasxDiasGrupoActividades(?,?,?,?)}"; //Editable
             //comando para ejecutar
             OdbcCommand cmd = new OdbcCommand(store, cn);
             //parametros del store
             OdbcParameter prm = cmd.Parameters.Add("pidIntervencion", OdbcType.Int);      //Editable       
             //valor del parametro
             prm.Value = IdIntervencion;
             //parametros del store
             OdbcParameter prm1 = cmd.Parameters.Add("pGrupoActividad", OdbcType.VarChar, 4);      //Editable       
             //valor del parametro
             prm1.Value = GrupoActividad;
             //parametros del store
             OdbcParameter prm2 = cmd.Parameters.Add("pFechaInicio", OdbcType.Date);      //Editable       
             //valor del parametro
             prm2.Value = FechaInicio;
             //parametros del store
             OdbcParameter prm3 = cmd.Parameters.Add("pFechaFin", OdbcType.Date);      //Editable       
             //valor del parametro
             prm3.Value = FechaFin;
             //pasar el comando al adapter
             OdbcDataAdapter da = new OdbcDataAdapter(cmd);
             //llenar el dataset
             da.Fill(dt);
             //reuperar en datatable
             dtResult = dt.Tables[0];
             //retornar
             return dtResult;

         }

         //funcion DSP para regresar las estadisticas en HORAS
         //GrupoActividad = OPN, OPC, OPA, ESP, 
         public DataTable HorasDiasGrupoActividades(int IdIntervencion, string Activo, string GrupoActividad, DateTime FechaInicio, DateTime FechaFin, bool DSP)
         {
             //en base al activo definir la bd a la que hace la consulta.
             string bd = DefinirBD(Activo);
             //datatable para el return
             DataTable dtResult = new DataTable();
             //datasource
             string sDataSourceID = "System.Sources.Db.Odbc.Mysql_ssipabk";
             //comando
             string sQueryID = "System.Sources.Db.Odbc.Mysql_ssipabk.Queries.sp_SelectHorasxDiasGrupoActividades.Query";
             //parametros
             IList<string> ilParameters = new List<string>();
             IRelationalDataService _dbQueryService = RelationalDataClientManager.RelationalDataService;
             //agregar parametro
             ilParameters.Add(bd);
             ilParameters.Add(IdIntervencion.ToString());
             ilParameters.Add(GrupoActividad);
             ilParameters.Add(FechaInicio.ToString());
             ilParameters.Add(FechaFin.ToString());

             //ejecutar comando
             dtResult = _dbQueryService.QueryForData(sDataSourceID, sQueryID, ilParameters);
             //retornar
             return dtResult;

         }


         //funcion LOCAL para regresar las fechas min y max de estadisticas
         public DataTable FechasMinMaxEstadisticas(int IdIntervencion, string Activo)
         {
             //en base al activo definir la bd a la que hace la consulta.
             string bd = DefinirBD(Activo);
             //conexion por ODBC
             OdbcConnection cn = conectarODBC();
             //dataset para llenado
             DataSet dt = new DataSet();
             ///datatable para result
             DataTable dtResult = new DataTable();
             //query para el store
             string store = "{call " + bd + ".sp_FechaMinMaxEstadisticas(?)}"; //Editable
             //comando para ejecutar
             OdbcCommand cmd = new OdbcCommand(store, cn);
             //parametros del store
             OdbcParameter prm = cmd.Parameters.Add("pidIntervencion", OdbcType.Int);      //Editable       
             //valor del parametro
             prm.Value = IdIntervencion;
             //pasar el comando al adapter
             OdbcDataAdapter da = new OdbcDataAdapter(cmd);
             //llenar el dataset
             da.Fill(dt);
             //reuperar en datatable
             dtResult = dt.Tables[0];
             //retornar
             return dtResult;

         }



         //funcion DSP para regresar las fechas min y max de estadisticas
         public DataTable FechasMinMaxEstadisticas(int IdIntervencion, string Activo, bool DSP)
         {
             //en base al activo definir la bd a la que hace la consulta.
             string bd = DefinirBD(Activo);
             //datatable para el return
             DataTable dtResult = new DataTable();
             //datasource
             string sDataSourceID = "System.Sources.Db.Odbc.Mysql_ssipabk";
             //comando
             string sQueryID = "System.Sources.Db.Odbc.Mysql_ssipabk.Queries.sp_FechaMinMaxEstadisticas.Query";
             //parametros
             IList<string> ilParameters = new List<string>();
             IRelationalDataService _dbQueryService = RelationalDataClientManager.RelationalDataService;
             //agregar parametro
             ilParameters.Add(bd);
             ilParameters.Add(IdIntervencion.ToString());
             //ejecutar comando
             dtResult = _dbQueryService.QueryForData(sDataSourceID, sQueryID, ilParameters);
             //retornar
             return dtResult;

         }



         //funcion LOCAL para regresar informacion basica de una intervencion
         public DataTable InfoIntervencion(int IdIntervencion, string Activo)
         {
             //en base al activo definir la bd a la que hace la consulta.
             string bd = DefinirBD(Activo);
             //conexion por ODBC
             OdbcConnection cn = conectarODBC();
             //dataset para llenado
             DataSet dt = new DataSet();
             ///datatable para result
             DataTable dtResult = new DataTable();
             //query para el store
             string store = "{call " + bd + ".sp_SelectInformacionBasicaIntervencion(?)}"; //Editable
             //comando para ejecutar
             OdbcCommand cmd = new OdbcCommand(store, cn);
             //parametros del store
             OdbcParameter prm = cmd.Parameters.Add("pidIntervencion", OdbcType.Int);      //Editable       
             //valor del parametro
             prm.Value = IdIntervencion;
             //pasar el comando al adapter
             OdbcDataAdapter da = new OdbcDataAdapter(cmd);
             //llenar el dataset
             da.Fill(dt);
             //reuperar en datatable
             dtResult = dt.Tables[0];
             //retornar
             return dtResult;

         }


         //funcion DSP para regresar informacion basica de una intervencion
         public DataTable InfoIntervencion(int IdIntervencion, string Activo, bool DSP)
         {
             //en base al activo definir la bd a la que hace la consulta.
             string bd = DefinirBD(Activo);
             //datatable para el return
             DataTable dtResult = new DataTable();
             //datasource
             string sDataSourceID = "System.Sources.Db.Odbc.Mysql_ssipabk";
             //comando
             string sQueryID = "System.Sources.Db.Odbc.Mysql_ssipabk.Queries.sp_SelectInformacionBasicaIntervencion.Query";
             //parametros
             IList<string> ilParameters = new List<string>();
             IRelationalDataService _dbQueryService = RelationalDataClientManager.RelationalDataService;
             //agregar parametro
             ilParameters.Add(bd);
             ilParameters.Add(IdIntervencion.ToString());
             //ejecutar comando
             dtResult = _dbQueryService.QueryForData(sDataSourceID, sQueryID, ilParameters);
             //retornar
             return dtResult;

         }





#endregion

         #region bitacora
        //MANEJO DE LA BITACORA

         //funcion LOCAL para regresar los registros de la bitcora de movimientos
         public DataTable ConsultarBitacora(int IdIntervencion, string Activo)
         {
             //en base al activo definir la bd a la que hace la consulta.
             string bd = DefinirBD(Activo);
             //conexion por ODBC
             OdbcConnection cn = conectarODBC();
             //dataset para llenado
             DataSet dt = new DataSet();
             ///datatable para result
             DataTable dtResult = new DataTable();
             //query para el store
             string store = "{call " + bd + ".sp_TabConsultaImagenesIntervencion(?,?)}"; //Editable
             //comando para ejecutar
             OdbcCommand cmd = new OdbcCommand(store, cn);
             //parametros del store
             OdbcParameter prm1 = cmd.Parameters.Add("pidIntervencion", OdbcType.Int);      //Editable       
             //valor del parametro
             prm1.Value = IdIntervencion;
             //pasar el comando al adapter
             OdbcDataAdapter da = new OdbcDataAdapter(cmd);
             //llenar el dataset
             da.Fill(dt);
             //reuperar en datatable
             dtResult = dt.Tables[0];
             //retornar
             return dtResult;

         }



         //funcion DSP para regresar los registros de la bitcora de movimientos
            public DataTable ConsultarImagenes(int IdIntervencion, string Activo, bool DSP)
         {
             //en base al activo definir la bd a la que hace la consulta.
             string bd = DefinirBD(Activo);
             //datatable para el return
             DataTable dtResult = new DataTable();
             //datasource
             string sDataSourceID = "System.Sources.Db.Odbc.Mysql_ssipabk";
             //comando
             string sQueryID = "System.Sources.Db.Odbc.Mysql_ssipabk.Queries.sp_SelectBitacora.Query";
             //parametros
             IList<string> ilParameters = new List<string>();
             IRelationalDataService _dbQueryService = RelationalDataClientManager.RelationalDataService;
             //agregar parametro
             ilParameters.Add(bd);            
             ilParameters.Add(IdIntervencion.ToString());
             //ejecutar comando
             dtResult = _dbQueryService.QueryForData(sDataSourceID, sQueryID, ilParameters);
             //retornar
             return dtResult;

         }


         # endregion



         //en base al activo definir la bd a la que hace la consulta.
        private string DefinirBD(string Activo)
        {
          string bd="rmsobdprueba";   //base de datos mysql de ssipabk
             if (Activo == "Litoral")
                 bd = "litoralbd"; // base de daros mysql de ssiplitoral
             return bd;
        }



        //conectar por ODBC a la bd
        //el ODBC sirve las dos bases de datos
         private OdbcConnection conectarODBC()
         {
                 OdbcConnection cn;                          
                cn = new OdbcConnection("dsn=SSIPABK;UID=root;PWD=GSG2013;");
                return cn;        
        }
        

    
    }
}
